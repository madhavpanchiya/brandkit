<?php
namespace App\Models\api;

use DB; 
use Illuminate\Database\Eloquent\Model;
use App\Models\Master;
use App\Models\Common; 

class BusinessModel extends Master
{
    private static $table_name = 'business';
    public function __construct() {
        parent::__construct();      
        $this->common_model=New Common; 
    }   

    /*public function getMyBusiness($params) 
        {
            $business_user_id   =   $params['user_id']; 
            $query  =   DB::table(static::$table_name) 
                        ->leftJoin('images AS i', 'i.image_id', '=', 'business.business_logo')
                        ->leftJoin('category AS c', 'c.category_id', '=', 'business.business_category')
                        ->select(
                            'business.business_name',
                            'business.business_phone_no',
                            'business.business_email',
                            'business.business_address',
                            'business.business_website',
                            'c.category_name',
                            'i.image_name',
                            'i.image_file_name',
                            'i.image_alt_tag'
                        )
                        ->where('business.is_delete',0)  
                        ->where('business.business_user_id',$business_user_id);  
            $data   = $query->get();    
            $cL= $this->common_model->validList($data);   
            $catList=[];
            $i=0;
            foreach($cL as $key=>$value){
                $catList[$i]=(array)$value;  
                if(!empty($value->business_logo) && !empty($value->image_name)){
                    $src = asset('assets/upload/images/original/'.$value->image_name);  
                    $catList[$i]['business_icon']   =   $src; 
                }
                $i++; 
            }  
            return $catList;            
        }*/
        /*Business Category List*/
    public function getBusinessCategoryList($params)
        { 
            if (empty($params)) { 
                return false;
            }    

            $filter = '';
            $limit          = 8 ;
            if(!empty($params['page']) && $params['page']>0){
                $page       =   $params['page'];
                $sp         =   ($page-1)*$limit;
            }else{
                $page       =   1 ;
                $sp         =   0 ;
            }

            if(!empty($params['search']) && $params['search']!=''){
                $filter .=  '  AND (bc.business_category_name LIKE "%'.$params['search'].'%" ) ';  
            } 
            $query = "SELECT 
                bc.business_category_id, 
                bc.business_category_name
            FROM business_category AS bc
            WHERE bc.is_delete=0 
            ".$filter."
            ORDER by bc.business_category_name ASC
            LIMIT ".$sp.",".$limit."  ";     
            $businessCategoryList = DB::select($query); 


            $tquery = " SELECT  COUNT(bc.business_category_id) as total
                FROM business_category AS bc
                WHERE bc.is_delete=0   
                ".$filter." ";     
            $total = DB::select($tquery);  
            $data =[
                'business_category_list' =>$businessCategoryList,
                'total'             =>ceil($total[0]->total),
                'total_page'        =>ceil($total[0]->total/$limit),
                'current_page'      =>(int)$page,
            ];
            return $data;
        }
    public function getMyBusinessList($params)
        {
            if (empty($params)) { 
                return false;
            }    

            $filter = '';
            $limit          = 8 ;
            if(!empty($params['page']) && $params['page']>0){
                $page       =   $params['page'];
                $sp         =   ($page-1)*$limit;
            }else{
                $page       =   1 ;
                $sp         =   0 ;
            }
            if(!empty($params['user_id']) && $params['user_id']!=''){
                $filter .=  '  AND (b.business_user_id ='.$params['user_id'].') ';  
            }
            if(!empty($params['search']) && $params['search']!=''){
                $filter .=  '  AND (b.business_name LIKE "%'.$params['search'].'%" ) ';  
            } 
 
            $assetUrl = asset('assets/upload/images/thumb/').'/';
            $assetOriginalUrl = asset('assets/upload/images/original/').'/';

            $query = "SELECT 
                b.business_id, 
                b.business_category, 
                b.business_name, 
                b.business_phone_no, 
                b.business_secondary_phone_no, 
                b.business_email, 
                b.business_address, 
                b.business_website, 
                c.business_category_name, 
                CONCAT('".$assetUrl."',i1.image_name) AS business_image, 
                CONCAT('".$assetOriginalUrl."',i1.image_name) AS business_original_image 
            FROM business AS b
            LEFT JOIN images    as i1 ON i1.image_id=b.business_logo
            LEFT JOIN business_category  as c ON c.business_category_id =b.business_category
            WHERE b.is_delete=0 
            ".$filter."
            LIMIT ".$sp.",".$limit." ";     
            $businessList = DB::select($query); 


            $tquery = " SELECT  COUNT(b.business_id) as total
                FROM business AS b
                LEFT JOIN images    as i1 ON i1.image_id=b.business_logo
                LEFT JOIN business_category  as c ON c.business_category_id=b.business_category
                WHERE b.is_delete=0   
                ".$filter." ";     
            $total = DB::select($tquery);  
            $data =[
                'business_list'     =>$businessList,
                'total'             =>ceil($total[0]->total),
                'total_page'        =>ceil($total[0]->total/$limit),
                'current_page'      =>(int)$page,
            ];
            return $data;
        }
    public function getBusinessDetail($params)
        {
            if (empty($params)) { 
                return false;
            }    

            $filter = '';  
            if(!empty($params['business_id']) && $params['business_id']>0){
                $filter .=  '  AND (b.business_id='.$params['business_id'].') AND (b.business_user_id='.$params['user_id'].') ';  
            }    
            $assetUrl = asset('assets/upload/images/thumb/').'/'; 
            $assetoriginalUrl = asset('assets/upload/images/original/').'/'; 
            $query = " SELECT 
                b.*, 
                c.category_name  as business_category_name,
                i1.image_id,  
                CONCAT('".$assetUrl."',i1.image_name) AS business_image, 
                CONCAT('".$assetoriginalUrl."',i1.image_name) AS business_original_image 
            FROM business AS b
            LEFT JOIN images    as i1 ON i1.image_id=b.business_logo
            LEFT JOIN category  as c ON c.category_id=b.business_category
            WHERE b.is_delete=0 
            ".$filter."
            LIMIT 1 ";     
            $businessDetails = DB::select($query); 
            
            $businessData = [];
            if (!empty($businessDetails) && !empty($businessDetails[0])) { 
                $businessData = $businessDetails[0];
            } 
            $returnData = $businessData;
            $data =[
                'business_detail'         =>$returnData
            ];
            return $data;
        }        
    
}
