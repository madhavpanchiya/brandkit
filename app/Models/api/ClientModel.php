<?php
namespace App\Models\api;

use DB; 
use Illuminate\Database\Eloquent\Model;
use App\Models\Master;
use App\Models\Common; 

class ClientModel extends Master
{
    private static $table_name = 'clients';
    public function __construct() {
        parent::__construct();      
        $this->common_model=New Common; 
    }   
 
    public function getClientList()
        {
             
            $assetUrl = asset('assets/upload/images/thumb/').'/';
            $assetOriginalUrl = asset('assets/upload/images/original/').'/';

            $query = "SELECT 
                p.client_id,  
                p.client_name,  
                p.client_position,
                CONCAT('".$assetUrl."',i1.image_name) AS client_image, 
                CONCAT('".$assetOriginalUrl."',i1.image_name) AS client_original_image 
            FROM clients AS p
            LEFT JOIN images as i1 ON i1.image_id=p.client_image
            WHERE p.is_delete=0 
            ORDER BY p.client_position DESC
            LIMIT 10 ";     
            $clientList = DB::select($query);

            $tquery = "SELECT  COUNT(p.client_id) as total
                FROM clients AS p
                LEFT JOIN images as i1 ON i1.image_id=p.client_image
                WHERE p.is_delete=0 ";     
            $total = DB::select($tquery);  
            $data =[
                'client_List'         =>$clientList,
            ];
            return $data;
        }   
}
