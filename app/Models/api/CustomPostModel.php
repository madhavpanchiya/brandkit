<?php
namespace App\Models\api;

use DB; 
use Illuminate\Database\Eloquent\Model;
use App\Models\Master;
use App\Models\Common; 

class CustomPostModel extends Master
{
    private static $table_name = 'post';
    public function __construct() {
        parent::__construct();      
        $this->common_model=New Common; 
    }   
 
    public function getCustomPostList($params)
        {
            if (empty($params)) { 
                return false; 
            }    

            $filter = '';
            $limit          = 8 ;
            if(!empty($params['page']) && $params['page']>0){
                $page       =   $params['page'];
                $sp         =   ($page-1)*$limit;
            }else{
                $page       =   1 ;
                $sp         =   0 ;
            }

            if(!empty($params['post_category']) && $params['post_category']>0){
                $filter .=  '  AND (p.post_category='.$params['post_category'].') ';  
            }
            if(!empty($params['post_date']) && $params['post_date']>0){
                $dateFormat = date('Y-m-d',strtotime($params['post_date']));
                $filter .=  '  AND (p.post_date LIKE "%'.$dateFormat.'%" ) ';   
            } 
            if(!empty($params['post_type']) && $params['post_type']!=''){
                $filter .=  '  AND (p.post_type LIKE "%'.$params['post_type'].'%" )';  
            } 
            if(!empty($params['search']) && $params['search']!=''){
                $filter .=  'AND (p.post_name LIKE "%'.$params['search'].'%" ) OR (p.post_search_keyword LIKE "%'.$params['search'].'%" )';  
            }  
 
            $assetUrl = asset('assets/upload/images/thumb/').'/';
            $assetOriginalUrl = asset('assets/upload/images/original/').'/';

            $query = "SELECT 
                p.post_id,  
                p.post_name,  
                p.post_package,
                p.post_type,
                p.custom_post_json,
                DATE_FORMAT(p.post_date,'%d/%m/%Y') AS post_date, 
                CASE WHEN p.post_status =1 THEN 'Active' WHEN p.post_status =0 THEN 'InAcive' END AS post_status_text,
                c.category_name  as post_category_name, 
                CONCAT('".$assetUrl."',i1.image_name) AS post_image, 
                CONCAT('".$assetOriginalUrl."',i1.image_name) AS post_original_image 
            FROM post AS p
            LEFT JOIN images as i1 ON i1.image_id=p.post_image
            LEFT JOIN category  as c ON c.category_id=p.post_category
            WHERE p.is_delete=0 AND p.is_custom_post='yes'  AND p.post_status =1 
            AND c.is_delete=0 
            ".$filter."
            ORDER BY p.post_id DESC
            LIMIT ".$sp.",".$limit." ";     
            $postList = DB::select($query);

            $tquery = "SELECT  COUNT(p.post_id) as total
                FROM post AS p
                LEFT JOIN images    as i1 ON i1.image_id=p.post_image
                LEFT JOIN category  as c ON c.category_id=p.post_category
                WHERE p.is_delete=0 AND p.post_status =1  
                ".$filter." ";     
            $total = DB::select($tquery);  
            $data =[
                'post_list'         =>$postList,
                'total'             =>ceil($total[0]->total),
                'total_page'        =>ceil($total[0]->total/$limit),
                'current_page'      =>(int)$page,
            ];
            return $data;
        }   
    public function getPostDetail($params)
        {
            if (empty($params)) { 
                return false;
            }    

            $filter = '';  
            if(!empty($params['post_id']) && $params['post_id']>0){
                $filter .=  '  AND (p.post_id='.$params['post_id'].') ';  
            }    
            $assetUrl = asset('assets/upload/images/thumb/').'/'; 
            $assetOriginalUrl = asset('assets/upload/images/thumb/').'/'; 
            $query = "SELECT 
                p.post_id,  
                p.post_name, 
                p.post_date,  
                p.post_package,
                CASE WHEN p.post_status =1 THEN 'Active' WHEN p.post_status =0 THEN 'InAcive' END AS post_status_text,
                c.category_name as post_category_name,
                i1.image_id, 
                CONCAT('".$assetUrl."',i1.image_name) AS post_image,
                CONCAT('".$assetOriginalUrl."',i1.image_name) AS post_original_image 
            FROM post AS p
            LEFT JOIN images  as i1 ON i1.image_id=p.post_image
            LEFT JOIN category  as c ON c.category_id=p.post_category
            WHERE p.is_delete=0 AND p.is_custom_post='yes' AND p.post_status =1
            ".$filter."
            LIMIT 1 ";     
            $postDetails = DB::select($query); 
            
            $postData = [];
            if (!empty($postDetails) && !empty($postDetails[0])) { 
                $postData = $postDetails[0];
            } 
            $returnData = $postData;
            $data =[
                'post_detail'         =>$returnData
            ];
            return $data;
        }        
    
}
