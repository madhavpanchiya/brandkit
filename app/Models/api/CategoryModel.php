<?php
namespace App\Models\api;

use DB; 
use Illuminate\Database\Eloquent\Model;
use App\Models\Master;
use App\Models\Common; 

class CategoryModel extends Master
{
    private static $table_name = 'category'; 
    public function __construct() {
        parent::__construct();      
        $this->common_model=New Common; 
    }   

    public function getCategoryList($params)
        {
            if (empty($params)) { 
                return false;
            }    

            $filter = '';
            $limit          = 8 ;
            if(!empty($params['page']) && $params['page']>0){
                $page       =   $params['page'];
                $sp         =   ($page-1)*$limit;
            }else{
                $page       =   1 ;
                $sp         =   0 ;
            }

            if(!empty($params['category_parent_id']) && $params['category_parent_id']>0){
                $filter .=  '  AND (c.category_parent_id ='.$params['category_parent_id'].') ';  
            }else{
                $filter .=  '  AND (c.category_parent_id = 0) ';  
            }

            /*if(!empty($params['category_type']) && $params['category_type']>0 && !empty($params['category_parent_id'])){
                $filter .=  '  AND (c.category_type='.$params['category_type'].') AND (c.category_parent_id != 0)';  
            }elseif(!empty($params['category_type']) && empty($params['category_parent_id'])){
                $filter .=  '  AND (c.category_type='.$params['category_type'].') AND (c.category_parent_id = 0)';  
            }*/ 
            if(!empty($params['search']) && $params['search']!=''){
                $filter .=  'AND (c.category_name LIKE "%'.$params['search'].'%" ) OR (c.category_search_keyword LIKE "%'.$params['search'].'%" )';
            } 
            
            $assetUrl = asset('assets/upload/images/thumb/').'/';
            $assetOriginalUrl = asset('assets/upload/images/original/').'/';

            $query = "SELECT 
                c.category_id, 
                c.category_parent_id, 
                c.category_name, 
                c.category_date, 
                c.category_description, 
                CASE WHEN c.category_type =1 THEN 'Post' WHEN c.category_type =2 THEN 'Video' END AS category_type,
                CONCAT('".$assetUrl."',i1.image_name) AS category_image, 
                CONCAT('".$assetOriginalUrl."',i1.image_name) AS category_original_image 
            FROM category AS c
            LEFT JOIN images    as i1 ON i1.image_id=c.category_image
            WHERE c.is_delete=0 AND c.category_status=1 
            ".$filter."
            LIMIT ".$sp.",".$limit." ";     
            $categoryList = DB::select($query); 
            if(!empty($categoryList)){
                foreach ($categoryList as $key => $value) {
                    $parent_name = DB::select("SELECT category_name FROM category WHERE category_id = ".$value->category_parent_id." AND is_delete=0 AND category_status=1");
                    if(!empty($parent_name[0]->category_name)){
                        $categoryList[$key]->parent_category_name = $parent_name[0]->category_name;
                    }else{
                        $categoryList[$key]->parent_category_name = '-';
                    }
                }
            }
            $tquery = " SELECT  COUNT(c.category_id) as total
                FROM category AS c
                LEFT JOIN images    as i1 ON i1.image_id=c.category_image
                WHERE c.is_delete=0 AND c.category_status=1  
                ".$filter." ";     
            $total = DB::select($tquery); 
            $data =[
                'category_list'     =>$categoryList,
                'total'             =>ceil($total[0]->total),
                'total_page'        =>ceil($total[0]->total/$limit),
                'current_page'      =>(int)$page,
            ];
            return $data;
        }
    /*Search Category*/
    public function getCategorySearchList($params)
        {
            if (empty($params)) { 
                return false;
            }
            $postList = [];
            $filter = '';
            $postfilter = '';
            $limit          = 5 ;
            if(!empty($params['page']) && $params['page']>0){
                $page       =   $params['page'];
                $sp         =   ($page-1)*$limit;
            }else{
                $page       =   1 ;
                $sp         =   0 ;
            }

            /*if(!empty($params['category_parent_id']) && $params['category_parent_id']>0){
                $filter .=  '  AND (c.category_parent_id ='.$params['category_parent_id'].') ';  
            }else{
                $filter .=  '  AND (c.category_parent_id = 0) ';  
            }*/
            $filter .=' AND (c.category_parent_id <> 0 )';  

            /*if(!empty($params['category_type']) && $params['category_type']>0 ){
                $filter .=  '  AND (c.category_type='.$params['category_type'].') AND (c.category_parent_id != 0)';  
            }elseif(!empty($params['category_type']) ){
                $filter .=  '  AND (c.category_type='.$params['category_type'].') ';  
            }*/ 
            if(!empty($params['search']) && $params['search']!=''){
                $search = strtolower( addslashes(strip_tags($params['search']) ) );

                if(strlen($search) < 2 ){
                    $temp_data =[
                        'post_list'         =>[],
                        'category_list'     =>[],
                        'total'             =>0,
                        'total_page'        =>1,
                        'current_page'      =>1,
                    ];
                    return $temp_data;
                }

                $filter .=  '  AND (c.category_name LIKE "%'.$search.'%" 
                            OR c.category_slug LIKE "%'.$search.'%" 
                            OR c.category_search_keyword LIKE "%'.$search.'%" 
                        )';  
                $postfilter .= ' AND (p.post_name LIKE "%'.$search.'%" 
                                OR p.post_search_keyword LIKE "%'.$search.'%" 
                                )';
            } 
            
            $assetUrl = asset('assets/upload/images/thumb/').'/';
            $assetOriginalUrl = asset('assets/upload/images/original/').'/';

            $query = "SELECT 
                c.category_id, 
                c.category_parent_id, 
                c.category_name, 
                c.category_date, 
                c.category_description, 
                CASE WHEN c.category_type =1 THEN 'Post' WHEN c.category_type =2 THEN 'Video' END AS category_type,
                (SELECT category_name FROM category WHERE category_id = c.category_parent_id
                AND is_delete=0 AND category_status=1) as parent_category_name,
                CONCAT('".$assetUrl."',i1.image_name) AS category_image, 
                CONCAT('".$assetOriginalUrl."',i1.image_name) AS category_original_image 
            FROM category AS c
            LEFT JOIN images  as i1 ON i1.image_id=c.category_image
            WHERE c.is_delete=0 AND c.category_status=1
            ".$filter."
            ORDER BY c.category_name ASC
            LIMIT ".$sp.",".$limit." ";     
            $categoryList = DB::select($query);

            /*Post Data*/
            $postquery = "SELECT 
                p.post_id,  
                p.post_name,  
                p.post_package,
                p.post_type,
                p.post_category,
                DATE_FORMAT(p.post_date,'%d/%m/%Y') AS post_date, 
                CASE WHEN p.post_status =1 THEN 'Active' WHEN p.post_status =0 THEN 'InAcive' END AS post_status_text,
                c.category_name  as post_category_name, 
                CONCAT('".$assetUrl."',i1.image_name) AS post_image, 
                CONCAT('".$assetOriginalUrl."',i1.image_name) AS post_original_image 
            FROM post AS p
            LEFT JOIN images as i1 ON i1.image_id=p.post_image
            LEFT JOIN category  as c ON c.category_id=p.post_category
            WHERE p.is_delete=0 AND p.is_custom_post='no' AND p.post_status =1
            ".$postfilter."
            ORDER BY p.post_id DESC
            LIMIT ".$sp.",".$limit." ";  
            $postData = DB::select($postquery);
            if(!empty($postData)){
                $postList[] = $postData;
            }

            /*if(!empty($categoryList)){
                foreach ($categoryList as $key => $value) {
                }
            }*/    
            // echo "<prE>"; print_r($categoryList);
            // echo "<prE>"; print_r(\Arr::flatten($postList));exit;

             $postcount = " SELECT  COUNT(p.post_id) as total
                FROM post AS p
                LEFT JOIN images as i1 ON i1.image_id=p.post_image
                LEFT JOIN category  as c ON c.category_id=p.post_category
                WHERE p.is_delete=0 AND p.is_custom_post='no' AND p.post_status =1  
               ".$postfilter." ";  
            $post_total = DB::select($postcount); 

            $tquery = " SELECT  COUNT(c.category_id) as total
                FROM category AS c
                LEFT JOIN images  as i1 ON i1.image_id=c.category_image
                WHERE c.is_delete=0  AND c.category_status=1  
                ".$filter." ";     
            $category_total = DB::select($tquery); 
            $temp_category_total  = $category_total[0]->total;
            $temp_postcount  = $post_total[0]->total;

            if( $temp_category_total > $temp_postcount ){
                $total = $temp_category_total;
            }else{
                $total = $temp_postcount;

            // print_r($total);exit();
            }
            $data =[
                'post_list'         =>\Arr::flatten($postList),
                // 'post_list'         =>$postList,
                'category_list'     =>$categoryList,
                'total'             =>ceil($total),
                'total_page'        =>ceil($total/$limit),
                'current_page'      =>(int)$page,
            ];
            return $data;
        }
    /*Custom Category*/    
    public function getCategoryCustomList($params)
        {
            if (empty($params)) { 
                return false;
            }    

            $filter = '';
            $limit          = 8 ;
            if(!empty($params['page']) && $params['page']>0){
                $page       =   $params['page'];
                $sp         =   ($page-1)*$limit;
            }else{
                $page       =   1 ;
                $sp         =   0 ;
            }

            /*if(!empty($params['category_parent_id']) && $params['category_parent_id']>0){
                $filter .=  '  AND (c.category_parent_id ='.$params['category_parent_id'].') ';  
            }else{
                $filter .=  '  AND (c.category_parent_id = 0) ';  
            }*/
            // $filter .=  'AND (c.category_parent_id = 0) AND (c.is_custom = 1)';  

            
            if(!empty($params['search']) && $params['search']!=''){
                $filter .=  '  AND (c.category_name LIKE "%'.$params['search'].'%" ) OR (c.category_search_keyword LIKE "%'.$params['search'].'%" )';  
            } 
            
            $assetUrl = asset('assets/upload/images/thumb/').'/';
            $assetOriginalUrl = asset('assets/upload/images/original/').'/';

            $paren_query = "SELECT 
                c.category_id, 
                c.category_name as category_parent_name, 
                c.category_date, 
            FROM category AS c 
            WHERE c.is_delete=0 AND c.category_status=1 AND (c.category_parent_id = 0) AND (c.is_custom = 1)
            LIMIT ".$sp.",".$limit." ";     
            $categoryList = DB::select($paren_query); 

            if(!empty($categoryList)){
                foreach ($categoryList as $key => $value) {
                    $categoryList[$key]->category_sub_data= DB::select("SELECT 
                        c.category_id,c.category_parent_id,c.category_name,c.category_description,
                        CASE WHEN c.category_type =1 THEN 'Post' WHEN c.category_type =2 THEN 'Video' END AS category_type,
                        CONCAT('".$assetUrl."',i1.image_name) AS category_image, 
                        CONCAT('".$assetOriginalUrl."',i1.image_name) AS category_original_image
                        FROM category AS c 
                        LEFT JOIN images    as i1 ON i1.image_id=c.category_image
                        WHERE c.category_status=1 AND c.category_parent_id =".$value->category_id.$filter." 
                        AND c.is_delete=0 ORDER BY c.category_name ASC ");
                }
            }
            $tquery = " SELECT  COUNT(c.category_id) as total
                FROM category AS c
                LEFT JOIN images    as i1 ON i1.image_id=c.category_image
                WHERE c.is_delete=0 AND c.category_status=1  
                ".$filter." ";     
            $total = DB::select($tquery); 
            $data =[
                'category_list'     =>$categoryList,
                'total'             =>ceil($total[0]->total),
                'total_page'        =>ceil($total[0]->total/$limit),
                'current_page'      =>(int)$page,
            ];
            return $data;
        }
    public function getCategoryDetail($params)
        {
            if (empty($params)) { 
                return false;
            }    

            $filter = '';  
            if(!empty($params['category_id']) && $params['category_id']>0 && !empty($params['category_parent_id'])){
                $filter .=  '  AND (c.category_id='.$params['category_id'].') AND (c.category_parent_id='.$params['category_parent_id'].') ';  
            }   
            $assetUrl = asset('assets/upload/images/thumb/').'/'; 
            $assetOriginalUrl = asset('assets/upload/images/original/').'/'; 
            $query = "SELECT 
                c.*, 
                CONCAT('".$assetUrl."',i1.image_name) AS category_image, 
                CONCAT('".$assetOriginalUrl."',i1.image_name) AS category_original_image 
            FROM category AS c
            LEFT JOIN images    as i1 ON i1.image_id=c.category_image
            WHERE c.is_delete=0 AND c.category_status=1
            ".$filter."
            LIMIT 1 ";     
            $categoryDetails = DB::select($query); 
            
            $categoryData = [];
            if (!empty($categoryDetails) && !empty($categoryDetails[0])) { 
                $categoryData = $categoryDetails[0];
            } 
            $returnData = $categoryData;
            $data =[
                'category_detail'       =>$returnData
            ];
            return $data;
        }        
    
}
