<?php

namespace App\Models\portal\master;

use DB;
use Illuminate\Database\Eloquent\Model;

class Custom_dashboard_model extends Model 
{
    private static $table_name = 'custom_dashboard';
    
    public function __construct() 
    {
        parent::__construct();
    }
 
    
    public static function dt_list_data($params = [])
    {
        if(empty($params)){
            return false;
        }
        $order_by           =   $params['order_by']; 
        $order_by_type      =   $params['order_by_type'];
        $limit_start        =   $params['limit_start'];
        $limit_length       =   $params['limit_length'];
        $where_raw          =   $params['where_raw'];

        $query = DB::table(static::$table_name)
                        ->select('custom_dashboard.*')
                        ->where('custom_dashboard.is_delete',0);

        if (!empty($where_raw)) {
            $query = $query->WhereRaw($where_raw);
        }
        if (!empty($order_by)) {
            $query = $query->orderBy($order_by,$order_by_type);
        }

        $total = $query->get()->count();
        $query = $query->limit($limit_length)->offset($limit_start); 
        $data = $query->get();
        return array('total'=>$total,"result"=>$data->toArray());
    }

    
    public static function get_edit_detail($passed_id = '')
        {
            $result = DB::table(static::$table_name)
                        ->select('custom_dashboard.*')
                        ->where('custom_dashboard.custom_dashboard_id',$passed_id)
                        ->where('custom_dashboard.is_delete',0)
                        ->first();
            if(!empty($result)){
                $data = (array)$result;
                    $custom_upcomingData = DB::table('custom_upcoming')
                        ->select('custom_upcoming.*')
                        ->where('custom_upcoming.custom_upcoming_dashboard_id',$passed_id)
                        ->where('custom_upcoming.is_delete',0)
                        ->get()->toArray();
                    if(!empty($custom_upcomingData)){
                        $data['custom_upcoming'] = $custom_upcomingData;
                    }else{
                        $data['custom_upcoming'] = [];
                    }    
                return $data;
            }
            return false;            
        }

    public static function check_custom_dashboard_exists($params = []){

        $result = DB::table(static::$table_name)
            ->where('is_delete',0)
            ->where($params)
            ->get()->count();

        if($result <= 0){
            return false;
        }
        return true;
    }



}
