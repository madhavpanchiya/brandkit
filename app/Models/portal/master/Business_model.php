<?php

namespace App\Models\portal\master;

use DB;
use Illuminate\Database\Eloquent\Model;

class Business_model extends Model
{
    private static $table_name = 'business';
    
    public function __construct()
    {
        parent::__construct();
    }

    public static function dt_list_data_byUser($params = [])
    {
        if(empty($params)){
            return false;
        }
        $userWhere = '';
        
        $order_by           =   $params['order_by'];
        $order_by_type      =   $params['order_by_type'];
        $limit_start        =   $params['limit_start'];
        $limit_length       =   $params['limit_length'];
        $where_raw          =   $params['where_raw'];


        if(!empty($params['user_id']) && $params['user_id']!=''){
            $userWhere .=  $params['user_id'];  
        }
        $query = DB::table(static::$table_name)
                        ->leftJoin('images','images.image_id','=','business.business_logo')
                        ->leftJoin('business_category','business_category.business_category_id','=','business.business_category')
                        ->select('business.*','business_category.business_category_name','images.image_name','images.image_url')
                        ;
                        // ->where('business.is_delete',0)

        if (!empty($userWhere)) {
            $query = $query->where('business.business_user_id',$userWhere);
        }
        if (!empty($where_raw)) {
            $query = $query->WhereRaw($where_raw);
        }
        if (!empty($order_by)) {
            $query = $query->orderBy($order_by,$order_by_type);
        }
        $total = $query->get()->count();
        $query = $query->limit($limit_length)->offset($limit_start); 
        $data = $query->get();
        return array('total'=>$total,"result"=>$data->toArray());
    }
    public static function dt_list_data($params = [])
    {
        if(empty($params)){
            return false;
        }
        $order_by           =   $params['order_by'];
        $order_by_type      =   $params['order_by_type'];
        $limit_start        =   $params['limit_start'];
        $limit_length       =   $params['limit_length'];
        $where_raw          =   $params['where_raw'];
        $query = DB::table(static::$table_name)
                    ->leftJoin('images','images.image_id','=','business.business_logo')
                    ->leftJoin('business_category','business_category.business_category_id','=','business.business_category')
                    ->select('business.business_id','business.business_user_id','business.is_delete','business.business_category','business.business_name','business.business_phone_no','business.business_secondary_phone_no','business.business_email','business.business_address','business.business_logo','business.created_at','business_category.business_category_name','images.image_name','images.image_url')
                    ;
                    // ->where('business.is_delete',0)

        if (!empty($where_raw)) {
            $query = $query->WhereRaw($where_raw);
        }
        if (!empty($order_by)) {
            $query = $query->orderBy($order_by,$order_by_type);
        }

        $total = $query->get()->count();
        $query = $query->limit($limit_length)->offset($limit_start); 
        $data = $query->get();
        return array('total'=>$total,"result"=>$data->toArray());
    }
    public static function get_edit_detail($passed_id = '')
        {
            $result = DB::table(static::$table_name)
                        ->leftJoin('images','images.image_id','=','business.business_logo')
                        ->leftJoin('category','category.category_id','=','business.business_category')
                        ->select('business.*','category.category_name','images.image_name','images.image_url','images.image_alt_tag')
                        // ->where('business.is_delete',0)
                        ->where('business.business_id',$passed_id)
                        ->first();

            return (array)$result;
        }
    public static function get_business_category_list()
        {
            $result = DB::table('business_category')
            ->select('business_category_id','business_category_name')
            ->where('is_delete', 0)
            ->orderBy('business_category_name')
            ->get()->toArray();
        
            return $result;
        }

    public static function check_business_exists($params = []){
        $result = DB::table(static::$table_name)
            ->where('is_delete',0)
            ->where($params)
            ->get()->count();

        if($result <= 0){
            return false;
        }
        return true;
    }




}
