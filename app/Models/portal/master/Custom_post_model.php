<?php

namespace App\Models\portal\master;

use DB;
use Illuminate\Database\Eloquent\Model;

class Custom_post_model extends Model 
{
    private static $table_name = 'post';
    
    public function __construct() 
    {
        parent::__construct();
    }

    
    public static function dt_list_data($params = [])
    { 
        if(empty($params)){
            return false;
        }
        $order_by           =   $params['order_by'];
        $order_by_type      =   $params['order_by_type'];
        $limit_start        =   $params['limit_start'];
        $limit_length       =   $params['limit_length'];
        $where_raw          =   $params['where_raw'];

        $query = DB::table(static::$table_name)
                        ->leftJoin('images','images.image_id','=','post.post_image')
                        ->leftJoin('category','category.category_id','=','post.post_category')
                        ->select('post.*','category.category_name','category.category_parent_id','images.image_name','images.image_url')
                        ->where('post.is_delete',0)
                        ->where('post.is_custom_post','=','yes');

        if (!empty($where_raw)) {
            $query = $query->WhereRaw($where_raw);
        }
        if (!empty($order_by)) {
            $query = $query->orderBy($order_by,$order_by_type);
        }
        $total = $query->get()->count();
        $query = $query->limit($limit_length)->offset($limit_start); 
        $data = $query->get();
        return array('total'=>$total,"result"=>$data->toArray());
    }
    
    public static function get_edit_detail($passed_id = '')
        {
            $result = DB::table(static::$table_name)
                        ->leftJoin('images','images.image_id','=','post.post_image')
                        ->leftJoin('category','category.category_id','=','post.post_category')
                        ->select('post.*','category.category_name','images.image_name','images.image_url','images.image_alt_tag')
                        ->where('post.post_id',$passed_id)
                        ->where('post.is_delete',0)
                        ->first();
            return (array)$result;
        }
    /*public static function getPostCount($params)
        {
            $query = DB::table('category')
                        ->select('category_id')
                        ->where('is_delete',0)
                        ->where('category_parent_id',$params);
            $data=$query->get();    
            // print_r($data);exit;
            // $query->get()->count();
        }*/    
    public static function check_post_exists($params = []){

        $result = DB::table(static::$table_name)
            ->where('is_delete',0)
            ->where($params)
            ->get()->count();

        if($result <= 0){
            return false;
        }
        return true;
    }

    public static function get_post_status($params = []){
        $result = DB::table($this->table_name)
            ->where('is_delete',0)
            ->where('post_status',1)
            ->where($params)
            ->get()->toArray();
        
        return $result;
    }




}
