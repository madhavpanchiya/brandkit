<?php
namespace App\Models\portal\master;

use DB;
use Illuminate\Database\Eloquent\Model;

class Business_category_model extends Model
{
    private static $table_name = 'business_category';
    
    public function __construct()
    {
        parent::__construct();
    }

    
    public static function dt_list_data($params = []) 
    {
        if(empty($params)){
            return false;
        }
        $order_by           =   $params['order_by'];
        $order_by_type      =   $params['order_by_type'];
        $limit_start        =   $params['limit_start'];
        $limit_length       =   $params['limit_length'];
        $where_raw          =   $params['where_raw'];

        $query = DB::table(static::$table_name)
                        ->select('business_category.business_category_id','business_category.business_category_name','business_category.created_at')
                        ->where('business_category.is_delete',0);

        if (!empty($where_raw)) {
            $query = $query->WhereRaw($where_raw);
        }
        if (!empty($order_by)) {
            $query = $query->orderBy($order_by,$order_by_type);
        }
        $total = $query->get()->count();
        $query = $query->limit($limit_length)->offset($limit_start); 
        $data = $query->get();
        return array('total'=>$total,"result"=>$data->toArray());
    }


    public static function get_ajax_list()
    {  
        $result = DB::table(static::$table_name)
            ->select('business_category_id','business_category_name')
            ->where('is_delete', 0)
            ->orderBy('business_category_name')
            ->get()->toArray();

        /*if(!empty($result)){
            foreach ($result as $key => $value) {
                $parent_name = DB::table(static::$table_name)->select('business_category_name')->where('business_category_id', $value->business_category_parent_id)->where('is_delete',0)->first();
                if(!empty($parent_name)){
                    $result[$key]->parent_business_category_name = $parent_name->business_category_name;
                }else{
                    $result[$key]->parent_business_category_name = '-';
                }
            }
        }*/    
        return $result;
    }
    public static function get_edit_detail($passed_id = '')
    {
        $result = DB::table(static::$table_name)
                        ->select('business_category.*')
                        ->where('business_category.business_category_id',$passed_id)
                        ->where('business_category.is_delete',0)
                        ->first();

        return (array)$result;
    }

    public static function check_business_category_exists($params = []){

        $result = DB::table(static::$table_name)
            ->where('is_delete',0)
            ->where($params)
            ->get()->count();

        if($result <= 0){
            return false;
        }
        return true;
    }

    



}
