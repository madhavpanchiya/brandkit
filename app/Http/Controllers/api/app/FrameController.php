<?php

namespace App\Http\Controllers\api\app;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController; 
use Illuminate\Http\Request; 
use Redirect;

use Illuminate\Support\Str;
/*Security & Session*/
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;

use Image;

 
/*Validation*/ 
use Illuminate\Support\Facades\Validator;
 
/*Loading Models Here*/  
use App\Models\api\UserModel; 
use App\Models\api\CategoryModel; 
use App\Models\api\FrameModel; 
use App\Models\Common; 

class FrameController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function __construct(){ 
        $this->common_model     = New Common; 
        $this->user_model       = New UserModel; 
        $this->frame_model       = New FrameModel;  
        $this->table_name       = 'frames';
    }
/*Category List*/
    public function frameList(Request $request) // Frame List 
        {
            $data = $request->all();
            $rules = [  
                'user_token'            => ['required','string','max:255'],
                "page"                  => ['required','integer','between:1,300000'],
                "search"                => ['string','max:255','nullable'], 
            ];
            
            $messages = []; 
            $validator = Validator::make($request->all(), $rules, $messages);  
            if($validator->fails()){
                return response()->json([
                    'message' =>\Arr::flatten($validator->errors()->toArray())[0],
                    'success' =>0,
                ], 200);
            }
            $userAuth =$this->user_model->authUser($data);    
            if (!empty($userAuth)) {
                $data['user_id']=$userAuth['user_id'];

                //\DB::table('user_track')->insert(['track_user_id'=>$userAuth['user_id'],'track_frame_id'=>$data[''] ]);

                $FrameList = $this->frame_model->getFrameList($data); 
                unset($userAuth['user_id']);
                unset($userAuth['session_id']);
                unset($userAuth['session_user_device_type']);
                unset($userAuth['session_expiry_timestamp']);
                unset($userAuth['session_status']);
                unset($userAuth['session_user_ip_address']);
                $arr                    =   array();
                $array['success']       =   1;          
                $array['message']       =   __('frame.frame_fetched');  
                $array['data']          =   $FrameList;  
                return response()->json($array, 200);
            }else{ 
                $arr                    =   array();
                $array['success']       =   2;          
                $array['message']       =   __('auth.invalid_access');  
                $array['data']          =   [];  
                return response()->json($array, 200);
            }
        }
   
    }
