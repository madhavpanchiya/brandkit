<?php

namespace App\Http\Controllers\api\app;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController; 
use Illuminate\Http\Request;
use Redirect;

use Illuminate\Support\Str; 
/*Security & Session*/
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;  
use Session;  

use Image;


/*Validation*/ 
use Illuminate\Support\Facades\Validator;
 
/*Loading Models Here*/  
use App\Models\api\UserModel; 
use App\Models\api\CustomPostModel; 
use App\Models\Common; 

class CustomPostController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function __construct(){ 
        $this->common_model     = New Common; 
        $this->user_model       = New UserModel; 
        $this->post_model       = New CustomPostModel; 
        $this->table_name       = 'post';
    }
/*My Post*/
    public function postList(Request $request) // post List 
        {
            $data = $request->all();
            $rules = [  
                'user_token'        => ['required','string','max:255'],
                "page"              => ['required','integer','between:1,300000'],
                "search"            => ['string','max:255','nullable'], 
                "post_category"     => ['integer','between:1,1111111111'],
                "post_date"         => ['nullable'],
                "post_type"         => ['nullable','string'],
            ];
            
            $messages = []; 
            $validator = Validator::make($request->all(), $rules, $messages);  
            if($validator->fails()){
                return response()->json([
                    'message' =>\Arr::flatten($validator->errors()->toArray())[0],
                    'success' =>0,
                ], 200); 
            }
            $userAuth =$this->user_model->authUser($data);    
            if (!empty($userAuth)) { 
                $data['user_id']=$userAuth['user_id'];
                
                $postList = $this->post_model->getCustomPostList($data); 
                unset($userAuth['user_id']);
                unset($userAuth['session_id']);
                unset($userAuth['session_user_device_type']);
                unset($userAuth['session_expiry_timestamp']);
                unset($userAuth['session_status']);
                unset($userAuth['session_user_ip_address']);
                $arr                    =   array();
                $array['success']       =   1;          
                $array['message']       =   __('post.post_fetched');  
                //$array['userData']      =   $userAuth;  
                $array['data']          =   $postList;  
                return response()->json($array, 200);
            }else{
                $arr                    =   array();
                $array['success']       =   2;          
                $array['message']       =   __('auth.invalid_access');  
                $array['data']          =   [];  
                return response()->json($array, 200);
            }
        }
    

}
