<?php

namespace App\Http\Controllers\api\app;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController; 
use Illuminate\Http\Request;
use Redirect;

use Illuminate\Support\Str; 
/*Security & Session*/
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;  
use Session;  

use Image;


/*Validation*/ 
use Illuminate\Support\Facades\Validator;
 
/*Loading Models Here*/  
use App\Models\api\UserModel; 
use App\Models\api\VideoModel; 
use App\Models\Common; 

class VideoController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function __construct(){ 
        $this->common_model     = New Common; 
        $this->user_model       = New UserModel; 
        $this->video_model      = New VideoModel; 
        $this->table_name       = 'videos';
    }
/*My Post*/
    public function videoList(Request $request) // post List 
        {
            $data = $request->all();
            $rules = [  
                'user_token'        => ['required','string','max:255'],
                "page"              => ['required','integer','between:1,300000'],
                "search"            => ['string','max:255','nullable'], 
                "video_category"     => ['integer','between:1,1111111111'],
                "video_date"         => ['nullable'],
                "video_type"         => ['nullable','string'],
            ];
            
            $messages = []; 
            $validator = Validator::make($request->all(), $rules, $messages);  
            if($validator->fails()){
                return response()->json([
                    'message' =>\Arr::flatten($validator->errors()->toArray())[0],
                    'success' =>0,
                ], 200); 
            }
            $userAuth =$this->user_model->authUser($data);    
            if (!empty($userAuth)) { 
                $data['user_id']=$userAuth['user_id'];

                /*\DB::table('user_track')->insert(['track_user_id'=>$userAuth['user_id'],'track_category_id'=>$data['video_category'] ]);//for track*/
                
                $videoList = $this->video_model->getVideoList($data); 
                unset($userAuth['user_id']);
                unset($userAuth['session_id']);
                unset($userAuth['session_user_device_type']);
                unset($userAuth['session_expiry_timestamp']);
                unset($userAuth['session_status']);
                unset($userAuth['session_user_ip_address']);
                $arr                    =   array();
                $array['success']       =   1;          
                $array['message']       =   __('video.video_fetched');  
                $array['data']          =   $videoList;  
                return response()->json($array, 200);
            }else{
                $arr                    =   array();
                $array['success']       =   2;          
                $array['message']       =   __('auth.invalid_access');  
                $array['data']          =   [];  
                return response()->json($array, 200);
            }
        }
        
    /*public function save_track(Request $request)
        {
            $data = $request->all();
            $rules = [  
                'user_token'            => ['required','string','max:255'],
                "post_id"               => ['required','integer','between:1,1111111111'],
                "frame_id"              => ['required','integer','between:1,1111111111'],
            ];
            $messages = []; 
            $validator = Validator::make($request->all(), $rules, $messages);  
            if($validator->fails()){
                return response()->json([
                    'message' =>\Arr::flatten($validator->errors()->toArray())[0],
                    'success' =>0,
                ], 200); 
            }
            $userAuth =$this->user_model->authUser($data);    
            if (!empty($userAuth)) { 
                $tack_data = [
                    'track_user_id'         => $userAuth['user_id'],
                    'track_category_id'     => $data['category_id'],
                    'track_post_save_id'    => $data['post_id'],
                    'track_frame_save_id'   => $data['frame_id'],
                ];

                \DB::table('user_track')->insert($tack_data);
                $arr                    =   array();
                $array['success']       =   1;          
                $array['message']       =   __('track_saved');  
                $array['data']          =  [];  
                return response()->json($array, 200);

            }else{

                $arr                    =   array();
                $array['success']       =   2;          
                $array['message']       =   __('auth.invalid_access');  
                $array['data']          =   [];  
                return response()->json($array, 200);
            }
        }*/    
    /*public function savePost(Request $request)
        {
            $data = $request->all();
            $rules = [  
                'user_token'            => ['required','string','max:255'],
                "video_category"         => ['required','integer','between:1,1111111111'],
                "post_name"             => ['required','string','max:255'],
                "post_package"          => ['required','string','max:255'],
            ];
            $messages = []; 
            $validator = Validator::make($request->all(), $rules, $messages);  
            if($validator->fails()){
                return response()->json([
                    'message' =>\Arr::flatten($validator->errors()->toArray())[0],
                    'success' =>0,
                ], 200); 
            }
            $userAuth =$this->user_model->authUser($data);    
            if (!empty($userAuth)) { 
                //image
                 $postData = [ 
                    'created_by'                => $userAuth['user_id'],
                    'video_category'             => $data['video_category'],
                    'post_name'                 => $data['post_name'],
                    'video_date'                 => $data['video_date'], 
                    'post_package'              => $data['post_package'], 
                    'post_status'               => $data['post_status'], 
                ];
                if($request->file('post_image')) {
                    $rand=rand(1111,9999);
                    $comman_time = time();
                    $hash1=md5($comman_time*$rand);
                    $originalImage= $request->file('post_image');  
                    $original_name = pathinfo($originalImage->getClientOriginalName(), PATHINFO_FILENAME);  
                    $image_new_name=$hash1.Str::slug($originalImage->getClientOriginalName(),'-').'.'.$originalImage->extension();
                     
                    $originalImage      = $request->file('post_image');
                    $thumbnailImage     = Image::make($originalImage->getRealPath());
                    $thumbnailPath      = public_path().'/assets/upload/images/thumb/';
                    $originalPath       = public_path().'/assets/upload/images/original/';  

                    $thumbnailImage->save($originalPath.$image_new_name); 
                    $thumbnailImage->resize(500, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $thumbnailImage->save($thumbnailPath.$image_new_name);   

                    $upload_image = [
                        'image_url'         =>  'assets/upload/images/original/'.$image_new_name,
                        'image_file_name'   =>  $original_name,
                        'image_name'        =>  $image_new_name ,
                        'image_details'     =>  'Post image',
                        'image_status'      =>  0,
                        'image_alt_tag'     =>  $data['post_name'],
                    ];
                    $image_id=\DB::table('images')->insertGetId($upload_image); 
                }else{
                    if(empty($data['post_id'])){
                        return response()->json([
                            'message' =>__('post.post_image_required'),
                            'success' => 0,
                        ], 200); 
                    }
                } 
                //End image
               
                if(empty($data['post_id']) ){
                    $postData['post_image']  = $image_id;
                    $postId = \DB::table($this->table_name)->insertGetId($postData);
                    if($postId>0){
                        $arr                    =   array();
                        $array['success']       =   1;          
                        $array['message']       =   __('post.post_saved');  
                        $array['data']          =   ['post_id'   =>  $postId];  
                        return response()->json($array, 200); 
                    }else{
                        $arr                    =   array();
                        $array['success']       =   0;          
                        $array['message']       =   __('post.post_saving_failed'); 
                        return response()->json($array, 200);
                    }
                }else{
                    if(empty($request->file('post_image')) ){
                        $postDetails = $this->video_model->getVideoDetail(['post_id'=>$data['post_id'] ]);
                        $postData['post_image']  = $postDetails['video_detail']->image_id;
                    }else{
                        $postData['post_image']  = $image_id;
                    }
                    $where      = ['post_id'=>$data['post_id']];
                    \DB::table($this->table_name)->where($where)->update($postData);
                    $arr                    =   array();
                    $array['success']       =   1;          
                    $array['message']       =   __('post.post_updated');  
                    $array['data']          =   ['post_id'   =>  $data['post_id']];  
                    return response()->json($array, 200); 
                }

                
            }else{
                $arr                    =   array();
                $array['success']       =   2;          
                $array['message']       =   __('auth.invalid_access');  
                $array['data']          =   [];  
                return response()->json($array, 200);
            }
        }*/
    /*public function removePost(Request $request)
        {
            $data = $request->all();
            $rules = [  
                'user_token'    => ['required','string','max:255'],
                "post_id"       => ['required','integer','between:1,1111111111'], 
            ];
            $messages = []; 
            $validator = Validator::make($request->all(), $rules, $messages);  
            if($validator->fails()){
                return response()->json([
                    'message' =>\Arr::flatten($validator->errors()->toArray())[0],
                    'success' =>0,
                ], 200); 
            }
            $userAuth =$this->user_model->authUser($data);    
            if (!empty($userAuth)) {
                $where = ['post_id'=>$data['post_id']];
                \DB::table($this->table_name)->where($where)->update(['is_delete'=>1]);
                $arr                    =   array();
                $array['success']       =   1;          
                $array['message']       =   __('post.post_delete');  
                $array['data']          =   [];  
                return response()->json($array, 200); 
            }else{
                $arr                    =   array();
                $array['success']       =   2;          
                $array['message']       =   __('auth.invalid_access');  
                $array['data']          =   [];  
                return response()->json($array, 200);
            }
        } */   


}
