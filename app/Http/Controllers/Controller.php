<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function save_json($params='')
        {
            $message = 'Data Saved Successfully';
            if(!empty($params)){
                $message = $params;
            }
            return response()->json(['status'=>200,'message'=>$message]);

        }

    public function update_json($params='')
        {
            $message = 'Data Updated Successfully';
            if(!empty($params)){
                $message = $params;
            }
            return response()->json(['status'=>200,'message'=>$message]);
            
        }

    public function error_json($params='')
        {
            $message = 'Oops! Try Later!';
            if(!empty($params)){
                $message = $params;
            } 
            return response()->json(['status'=>500,'message'=>$message]);
            
        }

    public function success_json($type='',$params='')
        {
            if(empty($type) || ($type != 'status' && $type != 'delete') ){
                $message = 'Success!';
            }elseif($type == 'status'){
                $message = 'Status Changed Successfully';
            }elseif($type == 'delete'){
                $message = 'Data Deleted Successfully';
            }

            if(!empty($params)){
                $message = $params;
            }
            return response()->json(['status'=>200,'message'=>$message]);
            
        }

    public function format_indian_time($passed_time) {
        date_default_timezone_set('Asia/Kolkata');
        return date('d-m-Y g:i A', strtotime($passed_time ) ) ;
       // return date('d-m-Y g:i A', strtotime('+5 hour +30 minutes', strtotime($passed_time) ) ) ;
    }

    public function mres($params)
        {
            $search = array("\\",  "\x00", "\n",  "\r",  "'",  '"', "\x1a");
            $replace = array("\\\\","\\0","\\n", "\\r", "\'", '\"', "\\Z");
        
            return str_replace($search, $replace, $params);
        }
    public function imageUpdate($product_image='',$iati1='',$image_type='',$image_details='')
        {
            $updateData=[
                'image_status'  =>1, 
                'image_type'    =>$image_type,
                'image_details' =>$image_details, 
            ];
            if (!empty($iati1)) {
                $updateData['image_alt_tag']=$iati1;
            }
            \DB::table('images')->where('image_id', $product_image)->update($updateData);
        }
}
