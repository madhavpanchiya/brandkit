<?php 
namespace App\Http\Controllers\portal\aexport;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController; 


/*Session & Security*/
use Illuminate\Support\Facades\Hash;

/*Loading Models Here*/ 
use App\Models\portal\master\User_model;

use App\Models\Common;

use Illuminate\Support\Str;
use Image;
use Excel;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithStyles;

/*Set this for get data in " get,post" in controller*/
use Illuminate\Http\Request;


class arrayExport implements FromArray,WithColumnWidths
{
    protected $exportData;

    public function __construct(array $exportData)
    {
        $this->exportData = $exportData;
    }

    public function array(): array
    {
        return $this->exportData;
    }
     
    public function columnWidths(): array
    {
        $row1       = $this->exportData[0][0];
        $all_rows   = $this->exportData[0];
        $row_count = count($row1); 

        $size_array = [];
        for ($i=0; $i <$row_count ; $i++) {  
            $max_length     =   0; 
            foreach ($all_rows as $key => $value) { 
                $len = strlen($value[$i]);
                if ($len > $max_length) {
                    $max_length = $len; 
                }
            }  
            $lenth          =   $max_length+2;


            // $lenth          =   strlen($row1[$i])+1;
            $size_array[$i] =   $lenth;             
        }  
 

        $return_array= [];
        for ($i=0; $i <$row_count ; $i++) {  
            $return_array[$this->getNameFromNumber($i)] = $size_array[$i];             
        }
        return $return_array;
    }

    public function getNameFromNumber($num) {
        $numeric = $num % 26;
        $letter = chr(65 + $numeric);
        $num2 = intval($num / 26);
        if ($num2 > 0) {
            return $this->getNameFromNumber($num2 - 1) . $letter;
        } else {
            return $letter;
        }
    }

    
} 

class InvoicesExport implements WithColumnWidths
{
    public function columnWidths(): array
    {
        return [
            'A' => auto,       
        ];
    }
} 


class ExportController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
     

    public function __construct(){
        // parent::__construct();    
        $this->user_model   = New User_model;  
        $this->common_model = New Common();  

    }


    public function index()
        { 
            $bList =  $this->user_model->businessList();
            $headers    = array_keys((array)$bList[0]);
            $body       = array_values((array)$bList[0]);
            
            $i=1;
            $body = [
                [
                    'ID',
                    'USER',
                    'PHONE NO',
                    'BUSINESS ID',
                    'BUSINESS NAME',
                    'BUSINESS PHONE 1',
                    'BUSINESS PHONE 2',
                    'BUSINESS EMAIL',
                    'BUSINESS ADDRESS',
                    'BUSINESS WEBSITE',
                    'BUSINESS CATEGORY',
                ]
            ];
            foreach($bList as $business){
                $body[$i] =array_values((array)$business);
                $i++;
            } 
             
            $coll = new arrayExport([$body]); 
            return Excel::download(
                $coll, 'Business List.xlsx'
            );  
 
        }

    public function export_response(Request $request)
        {  

            $data = array();
            $data['title']  ="Response Management";
            $data['active'] ="response";
            $data['sub']    ="response"; 


            $form_array = [
                [
                    'No',
                    'Place',
                    'Price',
                ],[
                    1,
                    'Adam',
                    '500',
                ],
            ];
            
            $coll = new arrayExport([$form_array]); 
            return Excel::download(
                $coll, 'report.xlsx'
            );  

             

            $table_body = array(); 
  

            
        }


 






public function strip_tags_blacklist($html) {
        $tags = explode('|','a|abbr|acronym|address|applet|area|article|aside|audio|b|base|basefont|bdi|bdo|big|blockquote|body|button|canvas|caption|center|cite|code|col|colgroup|datalist|dd|del|details|dfn|dialog|dir|div|dl|dt|em|embed|fieldset|figcaption|figure|font|footer|form|frame|frameset|h6|head|header|hr|html|i|iframe|img|input|ins|kbd|keygen|label|legend|li|link|main|map|mark|menu|menuitem|meta|meter|nav|noframes|noscript|object|ol|optgroup|option|output|param|pre|progress|q|rp|rt|ruby|s|samp|script|section|select|small|source|span|strike|strong|style|sub|summary|sup|table|tbody|td|textarea|tfoot|th|thead|time|title|tr|track|tt|u|ul|var|video|wbr');
        foreach ($tags as $tag) {
            $regex = '#<\s*' . $tag . '[^>]*>.*?<\s*/\s*'. $tag . '>#msi';
            $html = preg_replace($regex, '', $html);
        }
        return strip_tags($html);
    }

    public function mres($value)
        {
            $search = array("\\",  "\x00", "\n",  "\r",  "'",  '"', "\x1a");
            $replace = array("\\\\","\\0","\\n", "\\r", "\'", '\"', "\\Z");
        
            return str_replace($search, $replace, $value);
        }

 




}
