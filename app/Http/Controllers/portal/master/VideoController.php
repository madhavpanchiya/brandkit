<?php

namespace App\Http\Controllers\portal\master; 
 
use App\Http\Controllers\Controller; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator; 
use Illuminate\Support\Facades\Http;

use App\Models\portal\master\Video_model;
use App\Models\portal\master\Category_model;  
use App\Models\portal\master\Image_model;
use Image; 
use DB;

class VideoController extends Controller
{ 
    private $table_name; 
    private $view_title; 
    private $active;
    private $sub; 
    private $dt_table_display_name; 
    private $dt_table_small_name;
    private $route_name;
    private $add_edit_type;
    private $grid_add_button_name;
    private $grid_title;
    private $view_path;

    public function __construct(){

        $this->table_name = 'videos';
        $this->view_title = 'Video Management';  
        $this->active = 'video';
        $this->sub = 'video';
        $this->grid_title = 'Video List';
        $this->dt_table_display_name = 'Video';
        $this->dt_table_small_name = strtolower($this->dt_table_display_name);
        $this->route_name = 'video';
        $this->add_edit_type = ''; 
        $this->grid_add_button_name = 'Add '.$this->route_name;
        $this->view_path = 'portal/master/video/';
    }

    public function index()
        { 
            $data=['title'=>$this->view_title,'active'=>$this->active,'sub'=>$this->sub];
            return view('portal/master/master',compact('data')); 
        }

    public function dt_col()
        {
            $data=['title'=>$this->view_title,'active'=>$this->active,'sub'=>$this->sub];
            /*for filter*/ 
            $get_data='?';
            $data['parent_category_id'] = 0;
            if(!empty($_GET['parent_category_id'])){
                $get_data .= 'parent_category_id='.$_GET['parent_category_id'].'&';
                $data['parent_category_id'] = $_GET['parent_category_id'];
            }
            $data['return_url'] = url('/video-master');
            $data['filter_file'] = 'portal.master.video.filter';
            $grid_dt_url = url('/'.$this->route_name.'-list').$get_data;
            $data['sub_list'] = Category_model::get_ajax_list();
            /*End Filter*/
            /*Here we will use grid's data for making it dynamic*/ 
            $grid_columns = [
                [
                    'name'=>'No',
                    'width'=>'width="5%"',
                    'sortable'=>'true', 
                    'style'=>'style=""',
                    'class'=>'class="text-center"',
                ],
                [
                    'name'=> 'Categories',
                    'width'=>'width="15%"',
                    'sortable'=>'true',
                    'style'=>'style=""',
                    'class'=>'',
                ],
                [
                    'name'=> 'Name',
                    'width'=>'width="25%"',
                    'sortable'=>'true',
                    'style'=>'style=""',
                    'class'=>'',
                ],
                [
                    'name'=> 'Date',
                    'width'=>'width="15%"',
                    'sortable'=>'true',
                    'style'=>'style=""',
                    'class'=>'', 
                ],
                [
                    'name'=> 'Status',
                    'width'=>'width="15%"',
                    'sortable'=>'false',
                    'style'=>'style=""',
                    'class'=>'', 
                ],
                [
                    'name'=>'Action',
                    'width'=>'width="20%"',
                    'sortable'=>'false',
                    'style'=>'style=""',
                    'class'=>'', 
                ]
            ];

            $table_style='bVideo-collapse: collapse; bVideo-spacing: 0; width: -webkit-fill-available;';
            $table_class='table table-striped nowrap table-bVideoed dt-responsive nowrap';

            if($this->add_edit_type == 'model'){ 
                $data["extra_pages"] = ['portal/master/'.$this->route_name.'/add_modal'];
                $add_url = false;
            }else{
                $add_url = url('/'.$this->route_name.'-add');
            }

            $data['grid'] = [
                    'grid_name'             =>  $this->dt_table_display_name,
                    'grid_add_button'       =>  true,
                    'grid_add_button_name'  =>  $this->grid_add_button_name,
                    'grid_add_url'          =>  $add_url,
                    'grid_dt_url'           =>  $grid_dt_url,
                    'grid_delete_url'       =>  url('/'.$this->route_name.'-delete/'),
                    'grid_status_url'       =>  url('/'.$this->route_name.'-status/'),
                    'grid_data_url'         =>  url('/'.$this->route_name.'-edit/'), 
                    'grid_columns'          =>  $grid_columns,
                    'grid_order_by'         =>  '0',
                    'grid_order_by_type'    =>  'DESC',
                    'grid_tbl_name'         =>  $this->dt_table_small_name,
                    'grid_title'            =>  $this->grid_title,
                    'grid_tbl_display_name' =>  $this->dt_table_display_name,
                    'grid_tbl_length'       =>  '10',
                    'grid_tbl_style'        =>  $table_style,
                    'grid_tbl_class'        =>  $table_class
            ];
            return view('portal/master/master',$data); 
        }

    public function dt_list( $id = -1 )
        { 

            $start_index    = $_GET['iDisplayStart']!=null?$_GET['iDisplayStart']:0;
            $end_index      = $_GET['iDisplayLength']?$_GET['iDisplayLength']:10;      
            $search_text    = $_GET['sSearch']?$_GET['sSearch']:''; 
            $aColumns       = ['videos.video_id','videos.video_category','videos.video_name','videos.video_date','videos.video_type'];
            $aColumns_where = ['videos.video_id','videos.video_category','videos.video_name','videos.video_date','videos.video_type'];

            $order_by       = "";
            $where          = "";
            $order_by_type  = "DESC";

            if ( $_GET['iSortCol_0'] !== FALSE ){
                for ( $i=0 ; $i<intval($_GET['iSortingCols']); $i++ ){ if ($_GET['bSortable_'.intval($_GET['iSortCol_'.$i])] == "true" ){ $order_by = $aColumns[ intval( ( $_GET['iSortCol_'.$i] ) ) ]; $order_by_type = $this->mres( $_GET['sSortDir_'.$i] ); }
                }
            }

            for ( $i=0 ; $i<count($aColumns_where) ; $i++ ){ if ( isset($_GET['bSearchable_'.$i])  && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ){if($where != ''){$where .= " AND ";} $where .= $aColumns_where[$i]." = '".$this->mres($_GET['sSearch_'.$i])."' ";}
            }

            if( isset($_GET['sSearch'])  ){
                $where .= '('; $or = '';foreach( $aColumns_where as $row ){ $where .= $or.$row." LIKE '%".str_replace("'","\\\\\''",$this->mres($_GET['sSearch']))."%'"; if($or== ''){$or =' OR ';} }$where .= ')';
            }
            
             $filter='';
            // for filter 
            if(!empty($_GET['parent_category_id'])){
                $filter .= ' AND ( videos.video_category = '.$_GET['parent_category_id'].' ) ';
            }

            /*Get Data From Model*/
            $pass_data =   array(
                'limit_start'       =>  $start_index,
                'limit_length'      =>  $end_index,
                'where_raw'         =>  $where.$filter,
                "order_by"          =>  $order_by,
                "order_by_type"     =>  $order_by_type,
            );

            $all_data = Video_model::dt_list_data($pass_data);
            $data           = [];
            $i=$start_index;

            foreach( $all_data['result'] as $row ){
                $parent_category =  DB::table('category')->select('category.category_name')->where('category_id',$row->category_parent_id)->first()->category_name;
                $row_dt   = [];
                $row_dt[] = '# '.$row->video_id;
                $row_dt[] = $row->category_name.'<br>'.$parent_category;
                $row_dt[] = $row->video_name.'<br> <small>Type : '.ucfirst($row->video_type).'</small>';
                if(!empty($row->video_date)){
                    $row_dt[] = date('m-d-Y',strtotime($row->video_date));
                }else{
                    $row_dt[] = ' - ';
                }
                if ($row->video_status==1) { 
                    $row_dt[]= '<div style="cursor:pointer"  class="badge badge-success">Active</div>';
                    $status = '<i class="fa fa-ban"></i> &nbsp;&nbsp; InActive';
                    $status_type = 0;
                }else{ 
                    $row_dt[]= '<div style="cursor:pointer"  class="badge badge-danger">InActive</div>';
                    $status = '<i class="fa fa-check"></i> &nbsp;&nbsp; Active '; 
                    $status_type = 1;
                }
               
                $action = ''; 
                $action .= '<a class="dropdown-item" href="'.url('video-edit').'/'.$row->video_id.'"  title="Edit '.$this->route_name.'"> <i class="fa fa-edit"></i> &nbsp;&nbsp;Edit</a>';
                
                $action .= '<a class="dropdown-item"  href="#" onclick="js_delete('.$row->video_id.')"  title="Delete '.$this->route_name.'"> <i class="fa fa-trash"></i> &nbsp;&nbsp;Delete</a>';
                
                $action .= '<a class="dropdown-item" style="color:black !important;" href="javascript:;" onclick="js_status('.$row->video_id.','.$status_type.')">'.$status.'</a>';

                $row_dt[] = '<button class="btn btn-outline-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Action </button>
                        <div class="dropdown-menu">'.$action.'
                    </div>';
                
                $data[] = $row_dt;
            }

            $response['iTotalRecords'] = $response['iTotalDisplayRecords'] = $all_data['total'];
            $response['aaData'] = $data;
            
            return response()->json($response);
        }

    public function add()
        {
            $data = array();
            $data['category_list'] = Category_model::get_ajax_list_post();
            if($this->add_edit_type == 'model'){
                return redirect('/'.$this->route_name.'-master');
            }else{
                // echo "<pre>"; print_r($data['category_list']); exit;
                return view($this->view_path.'add',$data);
            }
        }

    public function edit($passed_id)
        { 
            $data = Video_model::get_edit_detail($passed_id);
            $data['category_list'] = Category_model::get_ajax_list_post();
            if($this->add_edit_type == 'model'){
                return view($this->view_path.'edit_modal',$data); 
            }else{
                return view($this->view_path.'edit',$data); 
            }
        }

    public function save(Request $request)
        {
            $params = $request->all();
            $data=array();
            $fields=array("video_category","video_name","video_date","video_package","video_type","video_search_keyword","video_image");
            foreach ($fields as $field) 
            {
                $data[$field]= \Arr::get($params, $field);
            }

            $id=\Arr::get($params, 'id');
            $mode=\Arr::get($params, 'mode');
            if ($mode=='add') {
                $validator = Validator::make($params, [
                    'video_category'=> 'required',
                    'video_name'    => 'required|string',
                    'video_date'    => 'required|string',
                    'video_type'    => 'required|string',
                    'video_file'    => 'required|mimetypes:video/mp4,video/avi,video/mpeg,video/quicktime',
                ]);
            }else{
                $validator = Validator::make($params, [
                    'video_category'=> 'required',
                    'video_name'    => 'required|string',
                    'video_date'    => 'required|string',
                    'video_type'    => 'required|string',
                ]);    
            }
            if($validator->fails()){
                return response()->json(['status'=>500,'message'=>\Arr::flatten($validator->errors()->toArray())[0]]);
            }
            $originalPath           = 'public/assets/upload/videos/'; 
            if(!is_dir($originalPath)){
                mkdir($originalPath, 0775, true);
            }
            
            if($request->hasFile('video_file')){
                $videofile = $request->file('video_file');
                $filenameWithExt= $videofile->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                $extension = $videofile->getClientOriginalExtension();
                $fileNameToStore = time().'_'.$filename.'.'.$extension;
                $videofile->move($originalPath,$fileNameToStore);
            }

            if($mode=='add') {
                   $data['video_file'] = $fileNameToStore;
                $inserted_id = \DB::table($this->table_name)->insertGetId($data);
                if($inserted_id){
                    if (!empty($data['video_image'])) { 
                      $this->imageUpdate($data['video_image'],'',1,'Video'); //Main image Update at insert
                    }
                }   
                return $this->save_json();
            }else{   
                if(!empty($_POST['old_video_file'])){
                    if(empty($request->file('video_file')))
                    {
                        $data['video_file'] = $_POST['old_video_file'];
                    }else{
                        unlink('public/assets/upload/videos/'.$_POST['old_video_file']);
                        $data['video_file'] = $fileNameToStore;
                    } 
                }

                if (!empty($data['video_image'])) { 
                  $this->imageUpdate($data['video_image'],'',1,'Video'); //Main image  Update at update
                }
                \DB::table($this->table_name)->where('video_id', $id)->update($data);
                return $this->update_json();
            } 
        }
    
        
    public function delete(Request $request)
        {    
            $params = $request->all();
            $id=\Arr::get($params, 'id');
            
            $is_updated = \DB::table($this->table_name)->where('video_id', $id)->update(['is_delete' => 1]);
            return $this->success_json('delete');
        }
    public function removeImages(Request $request)
        {
            $params = $request->all();
            $images_id=\Arr::get($params, 'images_id');
            $video_id=\Arr::get($params, 'video_id');
            if(!empty($images_id))
                {
                    $image_name = Image_model::get_edit_detail($images_id);
                    if(!empty($image_name['image_name'])){
                        unlink('assets/upload/images/original/'.$image_name['image_name']);
                        unlink('assets/upload/images/thumb/'.$image_name['image_name']);
                    }
                    $is_updated=\DB::table('images')->where('image_id', $images_id)->update(['image_status'=>0,'is_delete' => 1]);
                    if($is_updated){
                        return $this->success_json('delete');
                    }
                }
        }    
    public function status(Request $request)
    {
        $params = $request->all();
        $id=\Arr::get($params, 'id');
        $status=\Arr::get($params, 'status');

        $is_updated = \DB::table($this->table_name)->where('video_id', $id)->update(['video_status' => $status]);
        return $this->success_json('status');
    }


 




}
