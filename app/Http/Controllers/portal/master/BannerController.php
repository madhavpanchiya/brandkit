<?php

namespace App\Http\Controllers\portal\master; 
 
use App\Http\Controllers\Controller; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator; 
use Illuminate\Support\Facades\Http;
 
use App\Models\portal\master\Banner_model;
use App\Models\portal\master\Image_model;

class BannerController extends Controller
{  
    private $table_name; 
    private $view_title; 
    private $active; 
    private $sub;
    private $dt_table_display_name; 
    private $dt_table_small_name;
    private $route_name;
    private $add_edit_type;
    private $grid_add_button_name;
    private $grid_title;
    private $view_path;

    public function __construct(){

        $this->table_name = 'banners';
        $this->view_title = 'Banner Management';  
        $this->active = 'banner';
        $this->sub = 'banner';
        $this->grid_title = 'Banner List';
        $this->dt_table_display_name = 'Banner';
        $this->dt_table_small_name = strtolower($this->dt_table_display_name);
        $this->route_name = 'banner';
        $this->add_edit_type = 'model'; 
        $this->grid_add_button_name = 'Add '.$this->route_name;
        $this->view_path = 'portal/master/banner/';
    }

    public function index()
        { 
            $data=['title'=>$this->view_title,'active'=>$this->active,'sub'=>$this->sub];
            return view('portal/master/master',compact('data')); 
        }

    public function dt_col()
        {
            $data=['title'=>$this->view_title,'active'=>$this->active,'sub'=>$this->sub];
            /*Here we will use grid's data for making it dynamic*/ 
            $grid_columns = [
                [
                    'name'=>'No',
                    'width'=>'width="5%"',
                    'sortable'=>'true', 
                    'style'=>'style=""',
                    'class'=>'class="text-center"',
                ],
                [
                    'name'=> 'Name',
                    'width'=>'width="40%"',
                    'sortable'=>'true',
                    'style'=>'style=""',
                    'class'=>'',
                ],
                [
                    'name'=> 'Banner',
                    'width'=>'width="30%"',
                    'sortable'=>'false',
                    'style'=>'style=""',
                    'class'=>'', 
                ],
                [
                    'name'=>'Action',
                    'width'=>'width="25%"',
                    'sortable'=>'false',
                    'style'=>'style=""',
                    'class'=>'', 
                ]
            ];

            $table_style='bBanner-collapse: collapse; bBanner-spacing: 0; width: -webkit-fill-available;';
            $table_class='table table-striped nowrap table-bBannered dt-responsive nowrap';

            if($this->add_edit_type == 'model'){
                $data["extra_pages"] = ['portal/master/'.$this->route_name.'/add_modal'];
                $add_url = false;
            }else{
                $add_url = url('/'.$this->route_name.'-add');
            }

            $data['grid'] = [
                    'grid_name'             =>  $this->dt_table_display_name,
                    'grid_add_button'       =>  true,
                    'grid_add_button_name'  =>  $this->grid_add_button_name,
                    'grid_add_url'          =>  $add_url,
                    'grid_dt_url'           =>  url('/'.$this->route_name.'-list'),
                    'grid_delete_url'       =>  url('/'.$this->route_name.'-delete/'),
                    'grid_status_url'       =>  url('/'.$this->route_name.'-status/'),
                    'grid_data_url'         =>  url('/'.$this->route_name.'-edit/'), 
                    'grid_columns'          =>  $grid_columns,
                    'grid_order_by'         =>  '0',
                    'grid_order_by_type'    =>  'DESC',
                    'grid_tbl_name'         =>  $this->dt_table_small_name,
                    'grid_title'            =>  $this->grid_title,
                    'grid_tbl_display_name' =>  $this->dt_table_display_name,
                    'grid_tbl_length'       =>  '10',
                    'grid_tbl_style'        =>  $table_style,
                    'grid_tbl_class'        =>  $table_class
            ];
            return view('portal/master/master',$data); 
        }

    public function dt_list( $id = -1 )
        { 

            $start_index    = $_GET['iDisplayStart']!=null?$_GET['iDisplayStart']:0;
            $end_index      = $_GET['iDisplayLength']?$_GET['iDisplayLength']:10;      
            $search_text    = $_GET['sSearch']?$_GET['sSearch']:''; 
            $aColumns       = ['banners.banner_id','banners.banner_name','banners.banner_image'];
            $aColumns_where = ['banners.banner_id','banners.banner_name'];

            $order_by       = "";
            $where          = "";
            $order_by_type  = "DESC";

            if ( $_GET['iSortCol_0'] !== FALSE ){
                for ( $i=0 ; $i<intval($_GET['iSortingCols']); $i++ ){ if ($_GET['bSortable_'.intval($_GET['iSortCol_'.$i])] == "true" ){ $order_by = $aColumns[ intval( ( $_GET['iSortCol_'.$i] ) ) ]; $order_by_type = $this->mres( $_GET['sSortDir_'.$i] ); }
                }
            }

            for ( $i=0 ; $i<count($aColumns_where) ; $i++ ){ if ( isset($_GET['bSearchable_'.$i])  && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ){if($where != ''){$where .= " AND ";} $where .= $aColumns_where[$i]." = '".$this->mres($_GET['sSearch_'.$i])."' ";}
            }

            if( isset($_GET['sSearch'])  ){
                $where .= '('; $or = '';foreach( $aColumns_where as $row ){ $where .= $or.$row." LIKE '%".str_replace("'","\\\\\''",$this->mres($_GET['sSearch']))."%'"; if($or== ''){$or =' OR ';} }$where .= ')';
            }
            
            $filter='';
            
            /*Get Data From Model*/
            $pass_data =   array(
                'limit_start'       =>  $start_index,
                'limit_length'      =>  $end_index,
                'where_raw'         =>  $where.$filter,
                "order_by"          =>  $order_by,
                "order_by_type"     =>  $order_by_type,
            );

            $all_data = Banner_model::dt_list_data($pass_data);

            $data           = [];
            $i=$start_index;

            foreach( $all_data['result'] as $row ){
                $row_dt   = [];
                // $row_dt[] = ++$i;
                $row_dt[] = '#'.$row->banner_id;
                $row_dt[] = $row->banner_name;
                if(!empty($row->image_name)){
                    $row_dt[] = '<img src="'.url('assets/upload/images/thumb/'.$row->image_name).'" style="height: 80px; width: ; object-fit: cover;">';
                }else{
                    $row_dt[] = '';
                }
                
                $action = ''; 
                $action .= '<a class="dropdown-item" href="#" onclick="js_edit('.$row->banner_id.')"  title="Edit '.$this->route_name.'"> <i class="fa fa-edit"></i> &nbsp;&nbsp;Edit</a>';

                $action .= '<a class="dropdown-item"  href="#" onclick="js_delete('.$row->banner_id.')"  title="Delete '.$this->route_name.'"> <i class="fa fa-trash"></i> &nbsp;&nbsp;Delete</a>';

                $row_dt[] = '<button class="btn btn-outline-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Action </button>
                        <div class="dropdown-menu">'.$action.'
                    </div>';
                
                $data[] = $row_dt;
            }

            $response['iTotalRecords'] = $response['iTotalDisplayRecords'] = $all_data['total'];
            $response['aaData'] = $data;
            
            return response()->json($response);
        }

    public function add()
        {
            $data = array();
            if($this->add_edit_type == 'model'){
                return redirect('/'.$this->route_name.'-master');
            }else{
                return view($this->view_path.'add',$data);
            }
        }

    public function edit($passed_id)
        { 
            $data = Banner_model::get_edit_detail($passed_id);
            if($this->add_edit_type == 'model'){
                return view($this->view_path.'edit_modal',$data); 
            }else{
                return view($this->view_path.'edit',$data); 
            }
        }

    public function save(Request $request)
        {
            $params = $request->all();
            $data=array();
            $fields=array("banner_name","banner_title","banner_link","banner_text","banner_image");
            foreach ($fields as $field) 
            {
                $data[$field]= \Arr::get($params, $field);
            }

            $id=\Arr::get($params, 'id');
            $mode=\Arr::get($params, 'mode');

            $validator = Validator::make($params, [
                'banner_name'    => 'required|string',
            ]);

            if($validator->fails()){
                return response()->json(['status'=>500,'message'=>\Arr::flatten($validator->errors()->toArray())[0]]);
            }

            // $check_arr = array();
            // $check_arr[] = array('banner_name','=',$data['banner_name']);

            // if(!empty($id)){
            //     $check_arr[] = array('banner_id','<>',$id);
            // }
            // $banner_exists = Banner_model::check_banners_exists($check_arr);
            // if ($banner_exists) { 
            //     return response()->json(['status'=>500,'message'=>'Banner Already Exists']);
            // }
            if ($mode=='add') {
                $inserted_id = \DB::table($this->table_name)->insertGetId($data);
                if($inserted_id){
                    if (!empty($data['banner_image'])) { 
                      $this->imageUpdate($data['banner_image'],$_POST['iati1'],1,'Banner'); //Main image Update at insert
                    }
                }   
                return $this->save_json();
            }else{   
                if (!empty($data['banner_image'])) { 
                  $this->imageUpdate($data['banner_image'],$_POST['iati1'],1,'Banner'); //Main image  Update at update
                }
                \DB::table($this->table_name)->where('banner_id', $id)->update($data);
                return $this->update_json();
            } 
        }
    
        
    public function delete(Request $request)
        {    
            $params = $request->all();
            $id=\Arr::get($params, 'id');
            
            $is_updated = \DB::table($this->table_name)->where('banner_id', $id)->update(['is_delete' => 1]);
            return $this->success_json('delete');
        }



 




}
