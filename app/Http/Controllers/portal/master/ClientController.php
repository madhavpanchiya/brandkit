<?php

namespace App\Http\Controllers\portal\master; 
 
use App\Http\Controllers\Controller; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator; 
use Illuminate\Support\Facades\Http;
 
use App\Models\portal\master\Client_model;
use App\Models\portal\master\Image_model;

class ClientController extends Controller
{  
    private $table_name; 
    private $view_title; 
    private $active; 
    private $sub;
    private $dt_table_display_name; 
    private $dt_table_small_name;
    private $route_name;
    private $add_edit_type;
    private $grid_add_button_name;
    private $grid_title;
    private $view_path;

    public function __construct(){

        $this->table_name = 'clients';
        $this->view_title = 'Client Management';  
        $this->active = 'client';
        $this->sub = 'client';
        $this->grid_title = 'Client List';
        $this->dt_table_display_name = 'Client';
        $this->dt_table_small_name = strtolower($this->dt_table_display_name);
        $this->route_name = 'client';
        $this->add_edit_type = 'model'; 
        $this->grid_add_button_name = 'Add '.$this->route_name;
        $this->view_path = 'portal/master/client/';
    }

    public function index()
        { 
            $data=['title'=>$this->view_title,'active'=>$this->active,'sub'=>$this->sub];
            return view('portal/master/master',compact('data')); 
        }

    public function dt_col()
        {
            $data=['title'=>$this->view_title,'active'=>$this->active,'sub'=>$this->sub];
            /*Here we will use grid's data for making it dynamic*/ 
            $grid_columns = [
                [
                    'name'=>'No',
                    'width'=>'width="5%"',
                    'sortable'=>'true', 
                    'style'=>'style=""',
                    'class'=>'class="text-center"',
                ],
                [
                    'name'=> 'Name',
                    'width'=>'width="30%"',
                    'sortable'=>'true',
                    'style'=>'style=""',
                    'class'=>'',
                ],
                [
                    'name'=> 'Position',
                    'width'=>'width="20%"',
                    'sortable'=>'true',
                    'style'=>'style=""',
                    'class'=>'', 
                ],
                [
                    'name'=> 'Image',
                    'width'=>'width="20%"',
                    'sortable'=>'false',
                    'style'=>'style=""',
                    'class'=>'', 
                ],
                [
                    'name'=>'Action',
                    'width'=>'width="25%"',
                    'sortable'=>'false',
                    'style'=>'style=""',
                    'class'=>'', 
                ]
            ];

            $table_style='bClient-collapse: collapse; bClient-spacing: 0; width: -webkit-fill-available;';
            $table_class='table table-striped nowrap table-bCliented dt-responsive nowrap';

            if($this->add_edit_type == 'model'){
                $data["extra_pages"] = ['portal/master/'.$this->route_name.'/add_modal'];
                $add_url = false;
            }else{
                $add_url = url('/'.$this->route_name.'-add');
            }

            $data['grid'] = [
                    'grid_name'             =>  $this->dt_table_display_name,
                    'grid_add_button'       =>  true,
                    'grid_add_button_name'  =>  $this->grid_add_button_name,
                    'grid_add_url'          =>  $add_url,
                    'grid_dt_url'           =>  url('/'.$this->route_name.'-list'),
                    'grid_delete_url'       =>  url('/'.$this->route_name.'-delete/'),
                    'grid_status_url'       =>  url('/'.$this->route_name.'-status/'),
                    'grid_data_url'         =>  url('/'.$this->route_name.'-edit/'), 
                    'grid_columns'          =>  $grid_columns,
                    'grid_order_by'         =>  '0',
                    'grid_order_by_type'    =>  'DESC',
                    'grid_tbl_name'         =>  $this->dt_table_small_name,
                    'grid_title'            =>  $this->grid_title,
                    'grid_tbl_display_name' =>  $this->dt_table_display_name,
                    'grid_tbl_length'       =>  '10',
                    'grid_tbl_style'        =>  $table_style,
                    'grid_tbl_class'        =>  $table_class
            ];
            return view('portal/master/master',$data); 
        }

    public function dt_list( $id = -1 )
        { 

            $start_index    = $_GET['iDisplayStart']!=null?$_GET['iDisplayStart']:0;
            $end_index      = $_GET['iDisplayLength']?$_GET['iDisplayLength']:10;      
            $search_text    = $_GET['sSearch']?$_GET['sSearch']:''; 
            $aColumns       = ['clients.client_id','clients.client_name','clients.client_position',
                                'clients.client_image'];
            $aColumns_where = ['clients.client_id','clients.client_name','clients.client_position'];

            $order_by       = "";
            $where          = "";
            $order_by_type  = "DESC";

            if ( $_GET['iSortCol_0'] !== FALSE ){
                for ( $i=0 ; $i<intval($_GET['iSortingCols']); $i++ ){ if ($_GET['bSortable_'.intval($_GET['iSortCol_'.$i])] == "true" ){ $order_by = $aColumns[ intval( ( $_GET['iSortCol_'.$i] ) ) ]; $order_by_type = $this->mres( $_GET['sSortDir_'.$i] ); }
                }
            }

            for ( $i=0 ; $i<count($aColumns_where) ; $i++ ){ if ( isset($_GET['bSearchable_'.$i])  && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ){if($where != ''){$where .= " AND ";} $where .= $aColumns_where[$i]." = '".$this->mres($_GET['sSearch_'.$i])."' ";}
            }

            if( isset($_GET['sSearch'])  ){
                $where .= '('; $or = '';foreach( $aColumns_where as $row ){ $where .= $or.$row." LIKE '%".str_replace("'","\\\\\''",$this->mres($_GET['sSearch']))."%'"; if($or== ''){$or =' OR ';} }$where .= ')';
            }
            
            $filter='';
            
            /*Get Data From Model*/
            $pass_data =   array(
                'limit_start'       =>  $start_index,
                'limit_length'      =>  $end_index,
                'where_raw'         =>  $where.$filter,
                "order_by"          =>  $order_by,
                "order_by_type"     =>  $order_by_type,
            );

            $all_data = Client_model::dt_list_data($pass_data);


            $data           = [];
            $i=$start_index;

            foreach( $all_data['result'] as $row ){
                $row_dt   = [];
                // $row_dt[] = ++$i;
                $row_dt[] = '#'.$row->client_id;
                if(!empty($row->client_name)){
                     $client_name=$row->client_name;
                }else{  $client_name="-"; }    
                $row_dt[] = $client_name;  
                $row_dt[] = $row->client_position;
                if(!empty($row->image_name)){
                    $row_dt[] = '<img src="'.url('assets/upload/images/thumb/'.$row->image_name).'" style="height: 80px; width: ; object-fit: cover;">';
                }else{
                    $row_dt[] = '';
                }
                
                $action = ''; 
                $action .= '<a class="dropdown-item" href="#" onclick="js_edit('.$row->client_id.')"  title="Edit '.$this->route_name.'"> <i class="fa fa-edit"></i> &nbsp;&nbsp;Edit</a>';

                $action .= '<a class="dropdown-item"  href="#" onclick="js_delete('.$row->client_id.')"  title="Delete '.$this->route_name.'"> <i class="fa fa-trash"></i> &nbsp;&nbsp;Delete</a>';

                $row_dt[] = '<button class="btn btn-outline-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Action </button>
                        <div class="dropdown-menu">'.$action.'
                    </div>';
                
                $data[] = $row_dt;
            }

            $response['iTotalRecords'] = $response['iTotalDisplayRecords'] = $all_data['total'];
            $response['aaData'] = $data;
            
            return response()->json($response);
        }

    public function add()
        {
            $data = array();
            if($this->add_edit_type == 'model'){
                return redirect('/'.$this->route_name.'-master');
            }else{
                return view($this->view_path.'add',$data);
            }
        }

    public function edit($passed_id)
        { 
            $data = Client_model::get_edit_detail($passed_id);
            if($this->add_edit_type == 'model'){
                return view($this->view_path.'edit_modal',$data); 
            }else{
                return view($this->view_path.'edit',$data); 
            }
        }

    public function save(Request $request)
        {
            $params = $request->all();
            $data=array();
            $fields=array("client_name","client_position","client_image");
            foreach ($fields as $field) 
            {
                $data[$field]= \Arr::get($params, $field);
            }

            $id=\Arr::get($params, 'id');
            $mode=\Arr::get($params, 'mode');

            $validator = Validator::make($params, [
                'client_name'     => 'required|string',
                'client_image'    => 'required',
                'client_position' => 'required|integer|min:1',
            ]);

            if($validator->fails()){
                return response()->json(['status'=>500,'message'=>\Arr::flatten($validator->errors()->toArray())[0]]);
            }

            // $check_arr = array();
            // $check_arr[] = array('client_name','=',$data['client_name']);

            // if(!empty($id)){
            //     $check_arr[] = array('client_id','<>',$id);
            // }
            // $client_exists = Client_model::check_clients_exists($check_arr);
            // if ($client_exists) { 
            //     return response()->json(['status'=>500,'message'=>'Client Already Exists']);
            // }
            if ($mode=='add') {
                $inserted_id = \DB::table($this->table_name)->insertGetId($data);
                if($inserted_id){
                    if (!empty($data['client_image'])) { 
                      $this->imageUpdate($data['client_image'],'client_image',1,'Client'); 
                      //Main image Update at insert
                    }
                }   
                return $this->save_json();
            }else{   
                if (!empty($data['client_image'])) { 
                  $this->imageUpdate($data['client_image'],'client_image',1,'Client'); 
                  //Main image  Update at update
                }
                \DB::table($this->table_name)->where('client_id', $id)->update($data);
                return $this->update_json();
            } 
        }
    
        
    public function delete(Request $request)
        {    
            $params = $request->all();
            $id=\Arr::get($params, 'id');
            
            $is_updated = \DB::table($this->table_name)->where('client_id', $id)->update(['is_delete' => 1]);
            return $this->success_json('delete');
        }



 




}
