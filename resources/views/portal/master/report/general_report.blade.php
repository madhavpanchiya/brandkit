@extends('portal.template.app') 
@section('content')  
<?php 
$title = 'General Report';
$route_name = 'Report';

$mode = 'view';
$model_size = 'modal-lg';
?>
<style type="text/css">
  body > table{
    display: none;
  }
</style>
<div class="main-content">
  <section class="section">
    <div class="section-header">
        <h1><?php echo $title; ?></h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item "><a href="<?php echo url('portal');?>">Dashboard</a></div> 
            <div class="breadcrumb-item "><a href="<?php echo url('user-master');?>">Users</a></div> 
            <div class="breadcrumb-item active"><?php echo ucfirst($title); ?></div>
        </div>
    </div>
    <div class="section-body"> 
      <div class="row mt-sm-4"> 
        <div class="col-12 col-md-6 col-lg-6">
          <div class="card">
              <div class="card-header">
                 <h4 class="card-title">Most Use Post: </h4> 
              </div>
              <div class="card-body">
                <div class="row">
                  @if(!empty($mostuse['mostPostData']) )
                  @foreach($mostuse['mostPostData'] as $key=>$value)
                  <div class="col-md-3 col-12">
                    <div class=" card" style="; background: ;">
                      <div class="text-center">
                      </div>
                      <div class="card-body p-1">
                        <img src="<?php echo url($value->post_image);?>" style="height: 130px; object-fit: contain; width: 100%;" onclick="post({{$value->post_id}})">
                      </div>
                    </div>
                  </div>
                  @endforeach
                  @else
                  <div class="col-md-12 col-12">
                    <div class=" card" style="; background: ;">
                      <div class="text-center">
                        <h6>Most used post not available yet !</h6>
                      </div>
                    </div>
                  </div> 
                  @endif 
                </div>
              </div>
          </div>
        </div>
        <div class="col-12 col-md-6 col-lg-6">
          <div class="card">
              <div class="card-header">
                 <h4 class="card-title">Most Use Frame: </h4> 
              </div>
              <div class="card-body">
                <div class="row">
                  @if(!empty($mostuse['mostFrameData']) )
                  @foreach($mostuse['mostFrameData'] as $key=>$value)
                  <div class=" col-md-3 col-12">
                    <div class="card" style="border: ; background: ;">
                      <div class="text-center">
                      </div>
                      <div class="card-body p-1">
                        <img src="<?php echo url($value->frame_image);?>" style="height: 130px; object-fit: contain; width: 100%;" onclick="frame({{$value->frame_id}})">
                      </div>
                    </div>
                  </div>
                  @endforeach
                  @else
                  <div class="col-md-12 col-12">
                    <div class=" card" style="; background: ;">
                      <div class="text-center">
                        <h6>Most used Frame not available yet !</h6>
                      </div>
                    </div>
                  </div>
                  @endif 
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>    
  </section>
</div>
<script type="text/javascript">
  function post(id){
    window.open("<?php echo url('post-edit/'); ?>/"+id,'_blank');
  }function frame(id){
    window.open("<?php echo url('frame-edit/'); ?>/"+id,'_blank');
  }
</script>
@endsection

