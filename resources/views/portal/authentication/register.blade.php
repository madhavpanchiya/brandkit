@extends('portal.template.blank') 
@section('content') 
    <section class="section">
        <div class="container mt-5">
            <div class="row">
                <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4"> 
                    <div class="login-brand text-center"> 
                        <img style="height: 65px;" src="{{url('public/manager_template/img/reichFrontLogo.png')}}"> 
                    </div>
                    <div class="card card-primary">
                        <div class="card-header">
                            <h4>Register</h4> 
                        </div> 
                        <div class="card-body">
                           @include('messages.flash-message')

                           @php
                            if(Session::get('data')){
                                $default_data = Session::get('data'); 
                            } 
                           @endphp

                            <form method="POST" action="{{url('portal-do-register')}}" class="needs-validation" novalidate="" enctype="multipart/form-data">
                                @csrf
                               
                                <div class="form-group">
                                    <label for="user_name">Full Name</label>
                                    <input id="user_name" value="{{$default_data['user_name'] ?? '' }}" type="text" class="form-control" name="user_name" autofocus>
                                </div>  
                                <div class="form-group">
                                    <label for="user_email">Email</label>
                                    <input id="user_email" value="{{$default_data['user_email'] ?? '' }}" type="email" class="form-control" name="user_email">
                                    <div class="invalid-feedback">
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label for="user_password" class="d-block">Password</label>
                                    <input id="user_password" autocomplete="new-password" type="password" class="form-control pwstrength" data-indicator="pwindicator" name="user_password">
                                    <div id="pwindicator" class="pwindicator">
                                        <div class="bar"></div>
                                        <div class="label"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="user_confirm_password" class="d-block">Password Confirmation</label>
                                    <input id="user_confirm_password" type="password" class="form-control" name="user_confirm_password">
                                </div> 
                                <div class="form-divider">
                                    Your Profile Picture
                                </div>
                                 
                                <div class="form-group">
                                    <label>Profile Picture</label>
                                    <input type="file" name="user_profile_image" class="form-control">
                                </div>
                                     
                                <div class="form-group">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" name="agree" class="custom-control-input" id="agree">
                                        <label class="custom-control-label" for="agree">I agree with the <a href="{{url('terms-and-conditions')}}" target="_blank">terms and conditions</a></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-lg btn-block">
                                        Register
                                    </button>
                                </div>
                            </form> 
                        </div>
                    </div> 

                    <div class="  text-muted text-center">
                      Have an account? <a href="{{url('portal-login')}}">Login</a>
                    </div>
                </div>
            </div>
        </div>
    </section> 



@endsection