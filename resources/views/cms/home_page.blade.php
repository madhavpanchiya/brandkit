<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>BrandKit</title>

  <link rel="apple-touch-icon" sizes="180x180" href="{{asset('front_assets/assets/favicon/apple-touch-icon.png')}}">
  <link rel="icon" type="image/png" sizes="32x32" href="{{asset('front_assets/assets/favicon/favicon-32x32.png')}}">
  <link rel="icon" type="image/png" sizes="16x16" href="{{asset('front_assets/assets/favicon/favicon-16x16.png')}}">
  <link rel="manifest" href="{{asset('front_assets/assets/favicon/site.webmanifest')}}">

  <!--stylesheet-->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

  <link rel="stylesheet" href="{{asset('front_assets/assets/css/all.min.css')}}">
  
  <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css">
  
  <link rel="stylesheet" href="{{asset('front_assets/assets/css/style.css')}}">
</head>

<body>
<style type="text/css">
@media (max-width: 991.98px){
  .hero .hero-img::before {
      top: 0 !important;
      left: 40% !important;
  }
}
@media (max-width: 575.98px){
   .hero .hero-img::before {
      top: -25% !important;
      left: 50% !important;
      width: 101%;
      height: 97%;
  }
}

@media (max-width: 767.98px){
  .hero .main-heading {
    font-size: 4rem;
    margin-top: 8rem;
  }
}
.feature__box .icon {
    width: 17.2rem;
    height: 17.2rem;
}
@media (max-width: 768px){
  .growth__box .content {
      margin-top: 4rem;
      padding-left: 20px;
      padding-right: 20px;
  }
}
</style>
  <!--preloader start-->
  <div class="preloader">
      <div style="font-family: 'Poppins'; font-size: 15vw; color: black; font-weight: 900; position: absolute; top: 50%; left: 50%; -webkit-transform: translate(-50%, -50%); transform: translate(-50%, -50%); ">
        BrandKit
      </div>
  </div>
  <!--preloader end-->

  <!--header section start-->
  <header class="header header-1 p-3">
    <div class="container">
      <div class="header__wrapper">
        <div class="header__logo mx-md-0 mx-auto">
          <a href="{{url('/')}}" style=" font-size: 30px; color: black; font-weight: 900; ">
            BrandKit
          </a>
        </div>
      </div>
    </div>
  </header>
  <!--header section end-->

  <!--hero section start-->
  <section class="hero">
    <div class="hero__wrapper" style=" padding-top: 10rem; ">
      <div class="container">
        <div class="row align-items-lg-center m-0">
          <div class="col-lg-6 order-2 order-lg-1">
            <h1 class="main-heading color-black">Boost your online presence</h1>
            <p class="paragraph"><span>Want to create a festival and promotional posts to boost your business & your brand ? 
            The Best Way to Make Festival & daily posts for your Business is available Now!</p>
            <div class="download-buttons">
              <a href="https://play.google.com/store/apps/details?id=com.brandkit.post" class="google-play">
                <i class="fab fa-google-play"></i>
                <div class="button-content">
                  <h6>Download <span>Android app</span></h6>
                </div>
              </a>
            </div>
          </div>
          <div class="col-lg-6 order-1 order-lg-2">
            <div class="questions-img hero-img">
              <img src="{{ asset('front_assets/assets/images/phone-01.png'); }}" alt="image">
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--hero section end-->

  <!--feature section start-->
  <section class="feature" id="intro">
    <div class="container">
      <h2 class="section-heading color-black">Get surprised by amazing features.</h2>
      <div class="row">
        <div class="col-lg-3 col-md-6">
          <div class="feature__box feature__box--1">
            <div class="icon icon-1">
              <i class="fad fa-user-astronaut"></i>
            </div>
            <div class="feature__box__wrapper">
              <div class="feature__box--content feature__box--content-1">
                <h3>Join in Easy Steps</h3>
                <p class="paragraph dark">Super Easy to join the Band kit club> anyone with Indian number can join us.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6">
          <div class="feature__box feature__box--2">
            <div class="icon icon-2">
              <i class="fad fa-lightbulb-on"></i>
            </div>
            <div class="feature__box__wrapper">
              <div class="feature__box--content feature__box--content-2">
                <h3>Create your post</h3>
                <p class="paragraph dark">Choose From wide range of templates & frames. Create your custom post own your own or use our designed post.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6">
          <div class="feature__box feature__box--3">
            <div class="icon icon-3">
              <i class="fad fa-solar-system"></i>
            </div>
            <div class="feature__box__wrapper">
              <div class="feature__box--content feature__box--content-3">
                <h3>Boost Your brand</h3>
                <p class="paragraph dark">Share this post on whatsapp, Facebook, Instagram and in your Private Groups .</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6">
          <div class="feature__box feature__box--4">
            <div class="icon icon-4">
              <i class="fad fa-rocket-launch"></i>
            </div>
            <div class="feature__box__wrapper">
              <div class="feature__box--content feature__box--content-4">
                <h3>Become an Inspiration</h3>
                <p class="paragraph dark">With proper post design become Inspiration in circle & for upcoming Boomers.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--feature section end-->

  <!--video section start-->
<!--   <div class="video" id="video">
    <div class="video__wrapper">
      <div class="container">
        <div class="video__background">
          <img src="{{asset('front_assets/assets/images/video-bg-1.png')}}" alt="image" class="texture-1">
          <img src="{{asset('front_assets/assets/images/video-img.png')}}" alt="image" class="phone">
          <img src="{{asset('front_assets/assets/images/video-bg-2.png')}}" alt="image" class="texture-2">
        </div>
      </div>
    </div>
  </div> -->
  <!--video section end-->

  <!--growth section start-->
  <section class="growth" id="feature">
    <div class="growth__wrapper">
      <div class="container">
        <h2 class="section-heading color-black">Brandkit gives you exponential growth.</h2>
        <div class="row">
          <div class="col-lg-6">
            <div class="growth__box">
              <div class="icon">
                <i class="fad fa-user-astronaut"></i>
              </div>
              <div class="content">
                <h3>Start Easily</h3>
                <p class="paragraph dark">Signing up is really easy process. Anyone with android mobile can do it.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="growth__box">
              <div class="icon">
                <i class="fad fa-lightbulb-on"></i>
              </div>
              <div class="content">
                <h3>Improve Business</h3>
                <p class="paragraph dark">We help you maintain your online brand identity with latest content.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="growth__box">
              <div class="icon">
                <i class="fad fa-solar-system"></i>
              </div>
              <div class="content">
                <h3>Create Algorithms</h3>
                <p class="paragraph dark">Choose from wide range of templates & frame. Create your unique post.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="growth__box">
              <div class="icon">
                <i class="fad fa-backpack"></i>
              </div>
              <div class="content">
                <h3>Buy package</h3>
                <p class="paragraph dark">If you would like to have custom made frames for you or your company. Buy a package.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="growth__box">
              <div class="icon">
                <i class="fad fa-rocket-launch"></i>
              </div>
              <div class="content">
                <h3>Share Posts</h3>
                <p class="paragraph dark">All content of the app is free to share on any of your social media account.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="growth__box">
              <div class="icon">
                <i class="fad fa-user-astronaut"></i>
              </div>
              <div class="content">
                <h3>Quality Results</h3>
                <p class="paragraph dark">Now watch, how we help you get quality results in you business or brand.</p>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </section>
  <!--growth section end-->

  <!--step section start-->
  <!-- <section class="step">
    <div class="step__wrapper">
      <div class="container">
        <h2 class="section-heading color-black">Jumpstart your growth in just few clicks.</h2>
        <div class="row">
          <div class="col-lg-4">
            <div class="step__box">
              <div class="image">
                <img src="{{asset('front_assets/assets/images/phone-01.png')}}" alt="image">
              </div>
              <div class="content">
                <h3>EASY TO<span>Register.</span></h3>
                <p class="paragraph dark">Join the app in 3 easy steps and get
                  started with your progresso daily.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="step__box">
              <div class="image">
                <img src="{{asset('front_assets/assets/images/phone-02.png')}}" alt="image">
              </div>
              <div class="content">
                <h3>SIMPLE TO<span>Create.</span></h3>
                <p class="paragraph dark">Once you’re signed up you can create
                  as many polls you want to watch.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="step__box">
              <div class="image">
                <img src="{{asset('front_assets/assets/images/phone-03.png')}}" alt="image">
              </div>
              <div class="content">
                <h3>FUN TO<span>Measure.</span></h3>
                <p class="paragraph dark">Share your growth results with your
                  friends and inspre others.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section> -->
  <!--step section end-->

  <!--screenshot section start-->
  <!-- <section class="screenshot" id="preview">
    <div class="screenshot__wrapper">
      <div class="container">
        <div class="screenshot__info">
          <h2 class="section-heading color-black">Have a look at what’s inside the app.</h2>
          <div class="screenshot-nav">
            <div class="screenshot-nav-prev"><i class="fad fa-long-arrow-left"></i></div>
            <div class="screenshot-nav-next"><i class="fad fa-long-arrow-right"></i></div>
          </div>
        </div>
      </div>
      <div class="swiper-container screenshot-slider">
        <div class="swiper-wrapper">
          <div class="swiper-slide screenshot-slide">
            <img src="{{asset('front_assets/assets/images/phone-01.png')}}" alt="image">
          </div>
          <div class="swiper-slide screenshot-slide">
            <img src="{{asset('front_assets/assets/images/phone-02.png')}}" alt="image">
          </div>
          <div class="swiper-slide screenshot-slide">
            <img src="{{asset('front_assets/assets/images/phone-03.png')}}" alt="image">
          </div>
          <div class="swiper-slide screenshot-slide">
            <img src="{{asset('front_assets/assets/images/phone-04.png')}}" alt="image">
          </div>
          <div class="swiper-slide screenshot-slide">
            <img src="{{asset('front_assets/assets/images/phone-05.png')}}" alt="image">
          </div>
        </div>
      </div>
    </div>
  </section> -->
  <!--screenshot section end-->

  <!--footer start-->
  <footer class="footer">
    <div class="footer__wrapper" style="padding: 9rem 0 9rem;">
      <div class="container">
        <div class="row">
          <div class="col-lg-4">
            <div class="footer__info">
              <div class="footer__info--logo mb-4">
                <a href="/" style=" font-size: 30px; color: black; font-weight: 900; ">
                  BrandKit
                </a>
              </div>
              <div class="footer__info--content">
                <p class="paragraph dark">We understand your needs so, we create beautiful images with your logo and your business details to make your brand stand apart from your competitors. </p>
              </div>
              <div class="download-buttons">
                <h5>Download</h5>
                <a href="https://play.google.com/store/apps/details?id=com.brandkit.post" class="google-play">
                  <i class="fab fa-google-play"></i>
                  <div class="button-content">
                    <h6>Download <span>Android app</span></h6>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!--footer end-->

  <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
  <script>
    $(window).on('load', function () {
      $("body").addClass("loaded");
    });
  </script>
  <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
  <script src="{{asset('front_assets/assets/js/script.js')}}"></script>
</body>

</html>