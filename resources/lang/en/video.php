<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Language Lines for 
    |--------------------------------------------------------------------------
    | POST List api
    	add. edit. delete. view  
    */ 
/*POST*/    
        'video_saved'            =>  'Video has been saved successfully',
        'video_updated'          =>  'Video has been updated successfully',
        'video_fetched'          =>  'Video list has been fetched',
        'video_saving_failed'    =>  'Video saving failed', 
        'video_detail_fetched'   =>  'Video details fetched',
        'video_delete'           =>  'Data Deleted Successfully',

    /*POST Image Required*/
        'video_image_required'    => 'Video Image is compulsary required'
/*Category*/

];