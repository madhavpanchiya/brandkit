<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Language Lines for 
    |--------------------------------------------------------------------------
    | POST List api
    	add. edit. delete. view  
    */ 
/*POST*/    
        'post_saved'            =>  'Post has been saved successfully',
        'post_updated'          =>  'Post has been updated successfully',
        'post_fetched'          =>  'Post list has been fetched',
        'post_saving_failed'    =>  'Post saving failed', 
        'post_detail_fetched'   =>  'Post details fetched',
        'post_delete'           =>  'Data Deleted Successfully',

    /*POST Image Required*/
        'post_image_required'    => 'Post Image is compulsary required'
/*Category*/

];