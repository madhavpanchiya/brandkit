<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Language Lines for 
    |--------------------------------------------------------------------------
    | category api
    	list
    | post api
    	add. edit. delete. view  
    */ 


    /*Category API*/ 
        'category_sent'     =>  'Categories fetched successfully',  

    /*Post API (Business Ads)*/
        'accept_terms'          =>  'Please accept terms and condition using checkbox before proceeding',
        'post_saved'            =>  'Post has been saved successfully',
        'post_updated'          =>  'Post has been updated successfully',
        'post_fetched'          =>  'Post list has been fetched',
        'post_saving_failed'    =>  'Post saving failed', 
        'post_detail_fetched'   =>  'Post details fetched',

    /*Image 1 Required*/
        'image1_required'   =>  'Image 1 is compulsary required',
 




];
