 
<?php $__env->startSection('content'); ?>  
<?php 
$user_title = 'User';
$route_name = 'user';
$save_url = url($route_name.'-update');

$mode = 'view';
$model_size = 'modal-lg'; 
$business_title = "Business";
?>
<style type="text/css">
  body > table{
    display: none;
  }
</style>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/indrop/inlancer_drop.css')); ?>">

<div class="main-content">
  <section class="section">
    <div class="section-header">
        <h1><?php echo $user_title.' & '.$business_title; ?></h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="<?php echo url('portal');?>">Dashboard</a></div> 
            <div class="breadcrumb-item"><?php echo ucfirst($route_name); ?></div>
        </div>
    </div>
    <div class="section-body"> 
      <div class="row mt-sm-4"> 
        <div class="col-12 col-md-6 col-lg-7">
          <div class="card">
            <form id="user_edit" method="post" action="<?php echo e($save_url); ?>" class="needs-validation">
              <?php echo csrf_field(); ?>
              <div class="card-header">
                 <h4 class="card-title"><?php echo e($user_title); ?> - Info</h4> 
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="form-group col-md-6 col-12">
                      <label>User Name</label>
                      <input type="text" class="form-control" name="user_name" value="<?php echo e($user_detail['user_name']); ?>" >
                      <input type="hidden"  name="user_id" value="<?php echo e($user_detail['user_id']); ?>" >
                  </div> 
                  <div class="form-group col-md-6 col-12">
                      <label> Email</label>
                      <input type="email" class="form-control" name="user_email" value="<?php echo e($user_detail['user_email']); ?>" >
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-md-6 col-12">
                      <label>User Contact</label>
                      <input type="text" class="form-control" name="user_phone_no" value="<?php echo e($user_detail['user_phone_no']); ?>" >
                  </div> 
                  <div class="form-group col-md-6 col-12">
                      <label> Status</label>
                      <select name="user_status" class="form-control select2">
                        <?php if($user_detail['user_status'] == 1){ ?>
                          <option value="1" selected>Active</option>
                          <option value="0">InActive</option>
                          <option value="2">Block</option>
                        <?php }elseif($user_detail['user_status'] == 2){ ?>
                          <option value="2" selected>Block</option>
                          <option value="0">InActive</option>
                          <option value="1">Active</option>
                        <?php } else{ ?>
                          <option value="0" selected>InActive</option>
                          <option value="1">Active</option>
                          <option value="2">Block</option>
                        <?php } ?>
                      </select>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-md-6 col-12">
                      <label> Profile Picture </label>
                      <input type="file" name="user_profile_image" class="form-control">
                      <input type="hidden" name="current_user_profile_image"  value="<?php echo e($user_detail['user_profile_image']); ?>">
                  </div>
                  <div class="form-group col-md-6 col-12">
                     <?php if(!empty($user_detail['user_profile_image'])){?>
                      <img src="<?php echo e(asset('assets/upload/users/thumbnail/'.$user_detail['user_profile_image'])); ?>" style="height: 80px; width: ; object-fit: cover;">

                     <?php }else{ ?>
                      <img src="<?php echo e(asset('festival_inlancer/img/avatar/avatar-1.png')); ?>" style="height: 80px; width: ; object-fit: cover;">
                     <?php } ?>
                  </div> 
                  
                </div>
              </div>
              <br>
              <div class="card-footer text-right">
                  <button style="font-size: 15px !important;" type="submit" class="btn btn-success waves-effect waves-light"><i class="fa-pulse fa fa-spinner d-none"></i>Update User </button>&nbsp;  
              </div>  
            </form>
          </div>
        </div>
        <div class="col-12 col-md-6 col-lg-5">

          <div class="card">
              <div class="card-header">
                 <h4 class="card-title">Upload Frame</h4> 
              </div>
              <div class="card-body">
                <div class="row">

                  <div class="col-md-12">
                      <div id="i1" class="drop-area" data-throwback="storePerformance" onclick="document.getElementById('fileElem1').click()"> 
                          <div class="row justify-content-center" id="imgPreviewi1">
                            <i class="bi bi-cloud-arrow-up mb-3" style="color:#445dbe;font-size: 60px;"></i>
                            <h3 style="color: #445dbe;font-size: 18px;line-height: 100px;">Click "Here" or drop your frames here<span style="color:red">*</span></h3>
                          </div>
                          <input class="d-none" type="file" id="fileElem1" accept="" multiple="multiple" onchange="handleFiles(this.files,'i1','storePerformance')">   
                      </div>
                    <progress id="progress-bar" class="d-none" max=100 value=0></progress>
                  </div>
                  <div class="col-md-12">
                    <form class="form" method="post" id="add_frame_form" action="<?php echo e(url('frame-save')); ?>" enctype="multipart/form-data">
                        <input type="hidden" id="post_image" name="frame_list" data-image="i1">
                        <input type="text" name="frame_name" class="form-control d-none" placeholder="Enter Frame Name" >
                        <input type="hidden" value="premium" name="frame_package">
                        <input type="hidden" name="mode" value="add">
                        <input type="hidden" value=" " name="iati1" class="form-control">
                        <?php echo csrf_field(); ?>
                        <select name="assigned_user[]" multiple class="form-control d-none">
                          <option selected value="<?php echo e($user_detail['user_id']); ?>"><?php echo e($user_detail['user_name']); ?></option>
                        </select>
                        <input type="hidden" name="frame_data" id="json_aya_muko" value='[{"type":"name","value":[{"name":"show","value":"0"},{"name":"top","value":"439"},{"name":"left","value":"10"},{"name":"rotate","value":"0"},{"name":"font-size","value":"12"},{"name":"color","value":"#000000"}]},{"type":"logo","value":[{"name":"show","value":"0"},{"name":"top","value":"20"},{"name":"left","value":"20"},{"name":"rotate","value":"0"},{"name":"font-size","value":"12"},{"name":"color","value":"#000000"}]},{"type":"phone1","value":[{"name":"show","value":"0"},{"name":"top","value":"408"},{"name":"left","value":"10"},{"name":"rotate","value":"0"},{"name":"font-size","value":"12"},{"name":"color","value":"#000000"}]},{"type":"phone2","value":[{"name":"show","value":"0"},{"name":"top","value":"375"},{"name":"left","value":"10"},{"name":"rotate","value":"0"},{"name":"font-size","value":"12"},{"name":"color","value":"#000000"}]},{"type":"email","value":[{"name":"show","value":"0"},{"name":"top","value":"408"},{"name":"left","value":"167"},{"name":"rotate","value":"0"},{"name":"font-size","value":"12"},{"name":"color","value":"#000000"}]},{"type":"website","value":[{"name":"show","value":"0"},{"name":"top","value":"440"},{"name":"left","value":"167"},{"name":"rotate","value":"0"},{"name":"font-size","value":"12"},{"name":"color","value":"#000000"}]},{"type":"address","value":[{"name":"show","value":"0"},{"name":"top","value":"472"},{"name":"left","value":"10"},{"name":"rotate","value":"0"},{"name":"font-size","value":"12"},{"name":"color","value":"#000000"}]}]'>

                        <button type="submit" class="btn btn-success mt-2 d-none" id="save_frame">
                          <i class="fa-pulse fa fa-spinner"></i>Save Frames
                        </button>
                    </form>
                  </div>
                </div>
              </div>
          </div>

          <div class="card">
              <div class="card-header">
                 <h4 class="card-title">Assigned Frame</h4> 
              </div>
              <div class="card-body">
                <div class="row">
                  <?php if(!empty($frame_data) ): ?>
                  <?php $__currentLoopData = $frame_data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <div class="col-md-4 col-12">
                    <div>
                      <div class="text-center">
                      </div>
                      <div class="card-body p-1">
                        <img src="<?php echo url($value->frame_image);?>" style="height: 100px; object-fit: contain; width: 100%;">
                        <h6 class="text-center"><?php echo e($value->frame_name); ?></h6>
                      </div>
                    </div>
                  </div>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  <?php endif; ?> 
                </div>
              </div>
          </div>
                  


        </div>
      </div>
      <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
          <div class="card">
          	<div class="card-header">
          	   <h4 class="card-title"><?php echo e($business_title); ?></h4>
      	    </div>
            <div class="card-body">
          	  <form  name="business_form" id="business_form" method="post"> 
                <div class="table-responsive">
                  <table id='business_tbl' class="table table-striped table-loading" >
                    <thead>
                      <tr>
                    	  <?php foreach ($grid['grid_business_columns'] as $key => $value) {  ?>
                          <th <?php echo $value['width'] ?> <?php echo $value['style'] ?> <?php echo $value['class'] ?> > 
                            <?php echo $value['name'];?> 
                          </th> 
                        <?php  } ?> 
                      </tr>
                    </thead>
                  </table>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>    
  </section>
</div>

<script type="text/javascript">
/*User Update */  
jQuery(document).ready(function() {  
    var dd = {
        beforeSend: function() { 
            $('.fa-spinner').removeClass('d-none');
        },
        uploadProgress: function(event, position, total, percentComplete) { 
        },
        success: function() {},
        complete: function(response) {
            console.log(response.responseText);
            var result = jQuery.parseJSON(response.responseText);
            $('.fa-spinner').removeClass('d-none');
            $('.fa-spinner').addClass('d-none');
            if (result.status == 200) {
                Swal.fire({
                    type: 'success',
                    title: 'Successfully saved',
                    showConfirmButton: false,
                    timer: 1500
                });  
                setTimeout(function(){ location.reload(); }, 1500);
            } else {
                Swal.fire({
                    type: 'warning',
                    title: 'Oops',
                    text: result.message,
                    showConfirmButton: false,
                    timer: 2000
                });
            }
        },
        error: function() { 
        }
    }; 
    jQuery("#user_edit").ajaxForm(dd);
  });
/*End User Update */  
var is_first_time_load = true;
jQuery(document).ready(function(){ 
  <?php echo $grid['grid_business_tbl_name']; ?>_tbl = jQuery('#<?php echo $grid['grid_business_tbl_name']; ?>_tbl').dataTable({
      "oLanguage": {
          "sSearch": "Search",
          "sLengthMenu": "Show _MENU_ enteries",
          "sInfo":  " Showing  _START_  to  _END_  of  _TOTAL_  entries ", 
      },
      "language": {
        "paginate": {
          "previous": "Previous",
          "next": "Next",
        },
       "emptyTable": "No data available in table"
      },
      "processing": true,
      "fixedHeader": true,
      "serverSide": true, 
      "bAutoWidth": true, 
      "iDisplayLength": <?php echo $grid['grid_business_tbl_length']; ?>,
      "ajaxSource": "<?php echo $grid['grid_business_dt_url']?>",
      "aoColumns": [<?php foreach($grid['grid_business_columns'] as $key=>$value) { if($value['sortable']=='true'){ echo "{ 'bSortable' : true}," ;}  else {echo "{ 'bSortable' : false}," ;} }?>                     
      ],
      "order":[['<?php echo e($grid["grid_business_order_by"]); ?>','<?php echo e($grid["grid_business_order_by_type"]); ?>']],
      "sDom": "<'row'<'col-sm-9 col-xs-9'l><'col-sm-3 col-xs-3'f>r>t<'row'<'col-sm-5 hidden-xs paging-class'i><'col-sm-7 col-xs-12 clearfix'p>>",
      'fnDrawCallback' : function(){
        jQuery('th:first-child').removeClass('sorting_desc');
        jQuery('th:first-child').removeClass('sorting');
        if(is_first_time_load){
          jQuery('#<?php echo $grid['grid_business_tbl_name']; ?>_tbl_length select').addClass("form-control");
          jQuery('#<?php echo $grid['grid_business_tbl_name']; ?>_tbl_filter input').addClass("form-control"); 
          is_first_time_load = false;
        } 
      },
  });  
  jQuery('#<?php echo $grid['grid_business_tbl_name']; ?>_tbl').delay(100).css("width","100%");  
});
jQuery(document).ready(function() {
  jQuery('#<?php echo e($grid["grid_business_tbl_name"]); ?>_tbl').on('init.dt',function() {
        jQuery("#<?php echo e($grid["grid_business_tbl_name"]); ?>_tbl").removeClass('table-loading').show();
      });
  setTimeout(function(){
    jQuery('#<?php echo e($grid["grid_business_tbl_name"]); ?>_tbl').dataTable();
  }, 3000);
});

</script>
<script src="<?php echo e(asset('festival_inlancer/js/jquery.form.js')); ?>"></script>  
<script src="<?php echo e(asset('festival_inlancer/vendors/dataTable/datatables.min.js')); ?>"></script>






<script src="<?php echo e(asset('assets/indrop/inlancer_drop.js')); ?>"></script>

<script type="text/javascript">

function storePerformance(file_id,returnData) {    
    $("[data-image='"+file_id+"']").val($("[data-image='"+file_id+"']").val()+','+returnData.image_id);  
    $('#save_frame').removeClass('d-none');
    $('[name="frame_name"]').removeClass('d-none');
    $('.fa-pulse').addClass('d-none');
}

jQuery(document).ready(function() {

    var frame_dd = {
        beforeSend: function() { 
            $('.fa-spinner').removeClass('d-none');
        },
        uploadProgress: function(event, position, total, percentComplete) { 
        },
        success: function() {},
        complete: function(response) {
            var result = jQuery.parseJSON(response.responseText);
            $('.fa-spinner').removeClass('d-none');
            $('.fa-spinner').addClass('d-none');
            if (result.status == 200) {
                Swal.fire({
                    type: 'success',
                    title: result.message,
                    showConfirmButton: false,
                    timer: 1500
                });
                $('#add_form').trigger("reset");
                $(".select2").val('').trigger('change');
                // window.location.reload();
            } else {
                Swal.fire({
                    type: 'warning',
                    title: 'Oops',
                    text: result.message,
                    showConfirmButton: false,
                    timer: 2000
                });
            }
        },
        error: function() { 
        }
    };
    jQuery("#add_frame_form").ajaxForm(frame_dd);
});

</script>



<?php $__env->stopSection(); ?>


<?php echo $__env->make('portal.template.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/festival-app/resources/views/portal/master/user/user_master.blade.php ENDPATH**/ ?>