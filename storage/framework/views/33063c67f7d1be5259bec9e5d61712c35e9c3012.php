 
<?php $__env->startSection('content'); ?>  

<div class="main-content">
  	<section class="section">
	    <div class="section-header">
	        <h1>User Report</h1>
	        <div class="section-header-breadcrumb">
	            <div class="breadcrumb-item active"><a href="<?php echo url('portal');?>">Dashboard</a></div> 
	            <div class="breadcrumb-item">Report</div>
	        </div>
	    </div>
	    <div class="section-body"> 
	      	<div class="row mt-sm-4"> 
	        	<div class="col-12 col-md-6 col-lg-6">
	          		<div class="card">
	            		<form id="user_report" method="post" action="javascript:void(0);" class="needs-validation">
	              		<?php echo csrf_field(); ?>
		              	<div class="card-header">
		               		<h4 class="card-title">User Report</h4> 
		              	</div>
	              		<div class="card-body">
			                <div class="row">
			                  	<div class="form-group col-md-8 col-12">
			                        <select id="user_id" class="form-control select2" required>
                                        <option selected disabled>Select User Contact</option>
                                        <?php if(!empty($userList)): ?>
                                        <?php $__currentLoopData = $userList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        	<option value="<?php echo e($value->user_id); ?>">
                                        		<?php echo e($value->user_phone_no); ?> <?php if(!empty($value->user_name)): ?> - <?php echo e($value->user_name); ?> <?php endif; ?>
                                        	</option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </select>
			                    </div>
			                  	<div class="form-group col-md-4 col-12">
			                  		<div class=" text-right">
						                <button style="font-size: 15px !important;" type="submit" class="btn btn-success waves-effect waves-light"><i class="fa-pulse fa fa-spinner d-none"></i>View Report</button>&nbsp;  
						            </div>
			                  	</div>

			              	</div>
	              		</div>
	              		
	          			</form>
	      			</div>
	  			</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	jQuery(document).ready(function() {  
		jQuery("#user_report").submit(function (event) {
			user_id = $("#user_id").val();
			if(user_id !=null){
				var baseUrl = "<?php echo url('report-view/');?>/"+user_id;
				window.open(baseUrl,'_self');
			}else{
				Swal.fire({
                    type: 'warning',
                    title: 'Oops',
                    text: "Please Select User ",
                    showConfirmButton: false,
                    timer: 2000
                });
			}
		});
	});	
</script>
<script src="<?php echo e(asset('festival_inlancer/js/jquery.form.js')); ?>"></script>  
<?php $__env->stopSection(); ?>
<?php echo $__env->make('portal.template.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/festival-app/resources/views/portal/master/report/index.blade.php ENDPATH**/ ?>