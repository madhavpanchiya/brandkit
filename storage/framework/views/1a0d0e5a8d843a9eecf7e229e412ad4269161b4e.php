<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />   

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport"> 
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>" />
     
<!-- Favicon --> 
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo e(asset('festival_inlancer/favicon/favicon.png')); ?>">
    <!--<link rel="apple-touch-icon" sizes="57x57" href="<?php echo e(asset('festival_inlancer/favicon/apple-icon-57x57.png')); ?>">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo e(asset('festival_inlancer/favicon/apple-icon-60x60.png')); ?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo e(asset('festival_inlancer/favicon/apple-icon-72x72.png')); ?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo e(asset('festival_inlancer/favicon/apple-icon-76x76.png')); ?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo e(asset('festival_inlancer/favicon/apple-icon-114x114.png')); ?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo e(asset('festival_inlancer/favicon/apple-icon-120x120.png')); ?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo e(asset('festival_inlancer/favicon/apple-icon-144x144.png')); ?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo e(asset('festival_inlancer/favicon/apple-icon-152x152.png')); ?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo e(asset('festival_inlancer/favicon/apple-icon-180x180.png')); ?>">
    <link rel="icon" type="image/png" sizes="192x192" href="<?php echo e(asset('festival_inlancer/favicon/android-icon-192x192.png')); ?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo e(asset('festival_inlancer/favicon/favicon-32x32.png')); ?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo e(asset('festival_inlancer/favicon/favicon-96x96.png')); ?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo e(asset('festival_inlancer/favicon/favicon-16x16.png')); ?>">
    <link rel="manifest" href="<?php echo e(asset('festival_inlancer/favicon/manifest.json')); ?>">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo e(asset('festival_inlancer/favicon/ms-icon-144x144.png')); ?>">
    <meta name="theme-color" content="#ffffff"> -->
<!-- End Favicon -->
    <title><?php if(isset($data['title']) && $data['title']!=''): ?> <?php echo e($data['title']); ?> <?php else: ?> Portal <?php endif; ?> | <?php echo e(config('app.name')); ?></title>
        
    <!-- General CSS Files -->
    <!-- Styles --> 
    <link rel="stylesheet" href="<?php echo e(asset('festival_inlancer/modules/bootstrap/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('festival_inlancer//modules/fontawesome/css/all.min.css')); ?>">
    
    <!-- Template CSS -->
    <link rel="stylesheet" href="<?php echo e(asset('festival_inlancer/css/style.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('festival_inlancer/css/components.css')); ?>">
    <!-- Start GA -->
    <!-- /END GA -->

    <script src="<?php echo e(asset('festival_inlancer/modules/jquery.min.js')); ?>"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
</head>

<body>

    <div id="app"> 
        <?php echo $__env->yieldContent('content'); ?>   
    </div> 
    
    <script language="javascript">APPLICATION_URL="<?php echo e(asset('/')); ?>"</script>   
    <!-- General JS Scripts -->
    <script src="<?php echo e(asset('festival_inlancer/modules/popper.js')); ?>"></script>
    <script src="<?php echo e(asset('festival_inlancer/modules/tooltip.js')); ?>"></script>
    <script src="<?php echo e(asset('festival_inlancer/modules/bootstrap/js/bootstrap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('festival_inlancer/modules/nicescroll/jquery.nicescroll.min.js')); ?>"></script>
    <script src="<?php echo e(asset('festival_inlancer/modules/moment.min.js')); ?>"></script>
    <script src="<?php echo e(asset('festival_inlancer/js/stisla.js')); ?>"></script>
    
    <script src="<?php echo e(asset('festival_inlancer/js/scripts.js')); ?>"></script>
    <script src="<?php echo e(asset('festival_inlancer/js/custom.js')); ?>"></script>
     
    
</body>
 
</html> <?php /**PATH /var/www/festival-app/resources/views/portal/template/blank.blade.php ENDPATH**/ ?>