 
<?php $__env->startSection('content'); ?>

<link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/indrop/inlancer_drop.css')); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('festival_inlancer/vendors/summernote/summernote.css')); ?>">  

<?php 
$page_title = 'Edit Business';
$route_name = 'business'; 
$mode = 'edit';
$save_url = url($route_name.'-save');
$model_size = 'modal-lg';

?> 
<style type="text/css"> 
    textarea{
        height: unset;
    } 
</style>
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1><?php echo $page_title; ?></h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="<?php echo url('portal');?>">Dashboard</a></div> 
                <div class="breadcrumb-item"><?php echo ucfirst($route_name); ?></div>
            </div>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-12"> 
                    <div class="card">
                        <form class="form edit_form" method="post" id="edit_form" action="<?php echo e($save_url); ?>" enctype="multipart/form-data">
                            <input type="hidden" name="mode" value="<?php echo e($mode); ?>">
                            <input type="hidden" name="id" value="<?php echo e($business_id); ?>">
                            <?php echo csrf_field(); ?>
                            <div id="message"></div> 
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-xl-12 pb-5">
                                        <div class="card-body">
                                            <div id="accordion">
                                                <div class="accordion">
                                                    <div class="accordion-header collapsed" role="button" data-toggle="collapse" data-target="#edit-panel-body-1" aria-expanded="true">
                                                        <h4>Step-1 : Business Basic Info</h4>
                                                    </div>
                                                    <div class="accordion-body collapse show" id="edit-panel-body-1" data-parent="#accordion" style="">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="withdrawinput1">Business Category: </label>
                                                                            <select name="business_category" class="form-control select2">
                                                                                <option value="0" selected disabled>Select Category</option>
                                                                                <?php 
                                                                                if(!empty($businessCategory)){
                                                                                    foreach ($businessCategory as $category) { 
                                                                                        $selected = '';
                                                                                        if($category->business_category_id  == $business_category){
                                                                                            $selected = 'selected';
                                                                                        }?>
                                                                                        <option value="<?php echo e($category->business_category_id); ?>" <?php echo e($selected); ?>><?php echo e($category->business_category_name); ?></option>
                                                                                <?php } } ?>
                                                                            </select> 

                                                                        </div> 
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="withdrawinput1">Business Name: </label>
                                                                            <input type="text" name="business_name" id="business_name" class="form-control" value="<?php echo e($business_name); ?>">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="row">  
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="withdrawinput1">Business Contact: </label>
                                                                            <input type="text" name="business_phone_no" class="form-control" placeholder="Phone Number 1" value="<?php echo e($business_phone_no); ?>">
                                                                            <input type="text" name="business_secondary_phone_no" class="form-control mt-2" placeholder="Phone Number 2" value="<?php echo e($business_secondary_phone_no); ?>">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="row">  
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="withdrawinput1">Business Email: </label>
                                                                            <input type="text" name="business_email" class="form-control" placeholder="Ex. business@gmail.com" value="<?php echo e($business_email); ?>">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div><div class="col-md-6">
                                                                <div class="row">  
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="withdrawinput1">Website: </label>
                                                                            <input type="text" name="business_website" class="form-control" placeholder="Ex. www.business.com" value="<?php echo e($business_website); ?>">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="row">  
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="withdrawinput1">Business Address: </label>
                                                                            <textarea name="business_address" class="form-control"><?php echo e($business_address); ?></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="col-md-6">
                                                                <div class="row">
                                                                    <div class="col-md-12"> 
                                                                        <div class="form-group">
                                                                            <label for="withdrawinput1">Business Image Alt Tag: </label>
                                                                            <input type="text" name="iati1" class="form-control" value="<?php echo e($image_alt_tag); ?>">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- business_image -->
                                                            <div class="col-md-6">
                                                                <div id="i1" class="drop-area" data-throwback="storePerformance" onclick="document.getElementById('fileElem1').click()"> 
                                                                    <div class="text-center" id="imgPreviewi1">
                                                                      <i class="bi bi-cloud-arrow-up mb-3" style="color:#445dbe;font-size: 60px;"></i>
                                                                      <h3 style="color: #445dbe;font-size: 18px;line-height: 100px;">Click "Here" or drop your files here(size)<span style="color:red">*</span></h3>
                                                                    </div>
                                                                    <input class="d-none" type="file" id="fileElem1" accept="" onchange="handleFiles(this.files,'i1','storePerformance')">   
                                                                </div>
                                                              <progress id="progress-bar" class="d-none" max=100 value=0></progress>
                                                            </div>
                                                            <input type="hidden" id="business_logo" name="business_logo" data-image="i1" value="<?php echo e($business_logo); ?>">
                                                            <!-- business_image -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <button type="submit" class="btn btn-success waves-effect waves-light float-right"><i class="fa fa-spinner fa-spin d-none" tabindex="20"></i>Save</button>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                        </form>
                    </div>
                </div>
            </div>        
        </div>
    </section>
</div>
<script src="<?php echo e(asset('js/jquery.form.js')); ?>"></script>
<script src="<?php echo e(asset('assets/indrop/inlancer_drop.js')); ?>"></script>
<script src="<?php echo e(asset('festival_inlancer/vendors/summernote/summernote.min.js')); ?>"></script>

<script type="text/javascript">

/*image Show */
  var image = '<?php echo e($image_name); ?>';
  var file_id1 = 'i1';  
  if(typeof image!=='undefined' && image!=='') {
    var isrc = APPLICATION_URL+'/assets/upload/images/thumb/'+image;
    $('#imgPreview'+file_id1).empty();   
    var img = '<img onerror="setImage(this);"  class="previewImage" src="'+isrc+'">';  
    var div = '<div class="col-12"><div style="padding: 5px;margin-bottom: 8px;">'+img+'</div></div>';
    $('#imgPreview'+file_id1).append(div);    
  }
function setImage(img){
console.log(img);
    img.src="<?php echo e(url('/assets/img/placeholder.png')); ?>";  
    img.style.height="80px";
}
function storePerformance(file_id,returnData) {    
    $("[data-image='"+file_id+"']").val(returnData.image_id);  
}
/*End */

jQuery(document).ready(function() { 
    var dd = {
        beforeSend: function() { 
            $('.fa-spinner').removeClass('d-none');
        },
        uploadProgress: function(event, position, total, percentComplete) { 
        },
        success: function() {},
        complete: function(response) {
            var result = jQuery.parseJSON(response.responseText);
            $('.fa-spinner').removeClass('d-none');
            $('.fa-spinner').addClass('d-none');
            if (result.status == 200) {
                Swal.fire({
                    type: 'success',
                    title: result.message,
                    showConfirmButton: false,
                    timer: 1500
                });
                setTimeout(function(){
                    window.location.href= '<?php echo e(url($route_name."-master")); ?>';
                },2000); 
            } else {
                Swal.fire({
                    type: 'warning',
                    title: 'Oops',
                    text: result.message,
                    showConfirmButton: false,
                    timer: 2000
                });
            }
        },
        error: function() { 
        }
    };
    jQuery("#edit_form").ajaxForm(dd);
});

</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('portal.template.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/festival-app/resources/views/portal/master/business/edit.blade.php ENDPATH**/ ?>