/*Number Only Allowed In Input*/
	function numberOnlyAllowed(evt){
		var charCode=(evt.which?evt.which:evt.keyCode);

		if(charCode>=48 && charCode<=57 || charCode == 8 || charCode == 32 ||charCode == 37 ||charCode == 38  ||charCode == 38  || charCode == 46 || charCode == 9)
		{	
			return true;
		} 
		else {
			return false; 
		} 
	}

/*Select 2*/ 
  	if(jQuery().select2) {
	    jQuery(".select2").select2();
	    jQuery('.modal').on('shown.bs.modal', function (e) {
	    	jQuery(".select2-container").delay( 800 ).css("width","100%"); 
		})   
	}


jQuery(document).on('blur', "#phone_no", function () { 
    phone = jQuery("#phone_no").val(); 
    if (phone.length != 10)
    {	 
        jQuery('#phone_no').focus();   
        document.getElementById("phone_msg").textContent="Please Enter 10 Digit Phone Number";
    }else{
    	document.getElementById("phone_msg").textContent="";
    }  
});

jQuery(document).on('blur', ".phone_no", function () { 
    phone = jQuery(".phone_no").val(); 
    if (phone.length != 10)
    {	 
        jQuery('#booking_person_phone_no').focus();   
        document.getElementById("phone_msg").textContent="Please Enter 10 Digit Phone Number";
    }else{
    	document.getElementById("phone_msg").textContent="";
    }  
});

function valid_phone_number() { 
    var enter_number = $("#mobile_id").val();
    var phone_no = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
    var mobile_no = /^((\\+91-?)|0)?[0-9]{10}$/;
    var n = enter_number.length; 
    
    $("#typed_number").text(n);
    
    if( enter_number.toString().length > 10 ){
      if ((enter_number.match(phone_no)) || (enter_number.match(mobile_no))) { 
        return true;
      }
      else {
        window.location.hash="issue";  
        $("#warning").html("Enter Valid Mobile Number!");
        $("#mobile_id").focus();
          setTimeout(function(){
            $("#warning").html("&nbsp;");
        },5000);
        return false;
      }
    }
}

function submit_formaa() {

    var enter_number = $("#mobile_id").val();
    var phone_no = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
    var mobile_no = /^((\\+91-?)|0)?[0-9]{10}$/;
    if ((enter_number.match(phone_no)) || (enter_number.match(mobile_no))) {
      return true;
    }
    else {
      window.location.hash="issue";  
      $("#warning").html("Enter Valid Mobile Number!");
      $("#mobile_id").focus();
        setTimeout(function(){
          $("#warning").html("&nbsp;");
      },5000);
      return false;
    }
}

/*On Double ESP Press Refresh Page*/
	var delta = 800;
	var lastKeypressTime = 0;
	jQuery(document).keyup(function(e) {    
	  if (e.key == "Escape") {
	    var thisKeypressTime = new Date(); 
	    if (thisKeypressTime - lastKeypressTime <= delta) {
	      thisKeypressTime = 0;
	      location.reload();
	    }
	    lastKeypressTime = thisKeypressTime; 
	  } 
	});

/*Hotkeys Shortcut Master*/ 
	/*hotkeys('shift+p', function (event, handler){
	  switch (handler.key) { 
	    case 'shift+p': alert('HUrrayyy!');
	      break; 
	    default: alert(event);
	  }
	});*/


  /*Getting Distance Between 2 Lat Long*/
  function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2-lat1);  // deg2rad below
    var dLon = deg2rad(lon2-lon1); 
    var a = 
      Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
      Math.sin(dLon/2) * Math.sin(dLon/2)
      ; 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    var d = R * c; // Distance in km
    return d;
  }


  function copy_text(passed_value){
      var $temp = $("<input>");
      $("body").append($temp);
      $temp.val(passed_value).select();
      document.execCommand("copy");
      $temp.remove();
      swal.fire({
        toast: true,
        position: 'top-end',  
        type: "success",
        title: "Copied successfully",
        showConfirmButton: false,            
        timer: 1800
      });
  }




  /*Notification*/
    function getNotification()  
    {    
      $('#unread_notis').html(''); 
       
        var monthNames = ["January", "February", "March", "April", "May", "June",
          "July", "August", "September", "October", "November", "December"
        ];
        var no_noti_title   = 'No Notifications Yet';
        var no_noti_detail  = "We'll notify you when something arrives";
      
       

      var today = new Date();
      var tm = today.getMonth();
      var td = today.getDate();
      var ty = today.getFullYear();

      var URL=APPLICATION_URL+'/notifications';
      jQuery.ajax({
        headers: {
            'X-CSRF-TOKEN': csrf_token
        },
        type: "POST", 
        url: URL,
        cache: false,
        success: function(response) {
          var noti = JSON.parse(response);          
          var postf = 'Assigned';  
          var tString = 'Task';
          var fString = 'Form';
          
          if (noti.status==200) {  
 
            // var mycount= Object.keys(noti.notifications).length; 
 
            $('#noti_count_number').text(' ('+noti.count+')');
            if (typeof noti.notifications != 'undefined' && noti.count>0) {
              $('.notification-toggle').addClass('beep');
              $('.noti_btns').removeClass('d-none');
              var notifications = noti.notifications; 
              var i=1;
              notifications.forEach(function(noti) { 
                if (i<=4) { 
                  if (true) {}
                  var date = new Date(noti.created_at);
                  var month = date.getMonth();
                  var m     = date.getMonth();
                  var d     = date.getDate();
                  var y     = date.getFullYear();
                  var month = monthNames[m] 
                  var cur_d_class= '';
                  if (tm==m && td==d && ty==y) {
                    cur_d_class='text-primary'; 
                  }
                  if (noti.noti_type_id==1) {
                    var pref = tString;
                  }
                  if (noti.noti_type_id==3) {
                    var pref = fString;
                  }
                  
                  var n_html =`
                    <a href="#" class="dropdown-item noti_id_`+noti.noti_id+`"> 
                      <div class="dropdown-item-desc">
                        `+pref+' "'+noti.noti_title+'" '+postf+ `
                        <div class="time `+cur_d_class+`">`+month +' '+d+`</div>
                      </div>
                      <div onclick="removeNotification(`+noti.noti_id+`)" class="" style="justify-content: right; display: flex;margin: auto 0 auto auto;">
                        <svg class="p-cursor"  style="height: 0.7rem;margin:auto" fill="#37a000" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14 14" aria-hidden="true" role="img"><polygon points="12.524 0 7 5.524 1.476 0 0 1.476 5.524 7 0 12.524 1.476 14 7 8.476 12.524 14 14 12.524 8.476 7 14 1.476"></polygon></svg>
                      </div>
                    </a> 
                  `;
                  $('#unread_notis').append(n_html); 
                }

                i++;
              }); 
              $('.notification-toggle').addClass('beep');

            }else{ 
              var n_html =`
                <div style="margin-bottom: 8px;font-size: 1rem;" class="text-center"> 
                  <h5 class="up-green">`+no_noti_title+` !</h5><br>
                  <img style="height: 70px;" src="`+APPLICATION_URL+`/assets/images/notification.svg">
                  <br><br>
                  <p class="up-green">`+no_noti_detail+` !</p> 
                </div>
              `;
              $('#unread_notis').append(n_html);
              $('.noti_btns').addClass('d-none');
              $('.notification-toggle').removeClass('beep');

            } 
          }else{ 
          }
        } 
      });
      setTimeout(function(){  
        update_noti_color();
      }, 200);

    } 
  /*Notification END*/

  /*Update Noti Color*/
  function update_noti_color() { 
    var all_words = ["form", "task","Tarefa","Atribuído","Task","Formulário","Form"];
    all_words.forEach(function(word) { 
        var replacement = '<span class="up-green">' + word + '</span>';   
        var re = new RegExp(word, 'ig'); 
         
        $('.noti-list').each(function() {
            var text = $(this).html();
            $(this).html(text.replace(re, replacement)); 
        });  

    }); 
  } 
  /*End Update NOti Color*/
  /*Remove Notification*/
    function removeNotification(noti_id) {  
       $('.noti_id_'+noti_id).css('opacity','0.5');  
      $.ajax({
          headers: {
            'X-CSRF-TOKEN': csrf_token
          },
          type: "POST",
          data: {noti_id:noti_id},
          url: APPLICATION_URL+'/remove-noti',
          cache: false,
          success: function(response) {
              var json_data = jQuery.parseJSON(response);  
              $('#noti_count_number').text(' ('+json_data.count+')');
              if (json_data.status=="200") {   
                  if (json_data.count==0 || json_data.count<1) {
                      $('#no_noti').removeClass('d-none');
                      $('.notification-toggle').removeClass('beep');
                  }else{
                      $('.notification-toggle').addClass('beep');
                  }
                  iziToast.warning({
                      title: 'Removed!',
                      message: json_data.message,
                      position: 'topRight',
                      timeout: 1400,
                  }); 
                   $('.noti_id_'+noti_id).remove(); 
              }else{
                  alert('failed');
              }
          }
      }); 
    }

  /*End Remove Notification*/



jQuery(document).ready(function() {  
  /*setTimeout(function(){ 
    getNotification(); 
  }, 300);*/
  
});